#!/usr/bin/env python3

import argparse
from functools import partial
import glob
import multiprocessing
import os
import sys
import time

import pandas as pd
import numpy

from ost import GetVerbosityLevel, PushVerbosityLevel
from ost import conop
import ost

from PDBValidation.Mapping import *
from PDBValidation.PDBXReader import *
from PDBValidation.Residue import *
from PDBValidation.Validation import *
from PDBValidation.ValidationFactory import *
from PDBValidation.XML import *
from PDBValidation.utils import *

def parse_args():
    parser = argparse.ArgumentParser(description='Process a PDB Validation XML file.')
    parser.add_argument('-v', '--verbose', dest='verbosity',  action='append_const',
                        const=1, default = [GetVerbosityLevel()],
                        help = "Increase verbosity. Can be used multiple times.")
    parser.add_argument('-q', '--quiet', dest='verbosity',  action='append_const',
                        const = -1,
                        help = "Decrease verbosity. Can be used multiple times.")
    parser.add_argument('-n', '--ncores', default = guessCores(), type = int,
                        help = "Number of parallel processes to use.")
    parser.add_argument('-d', '--distance', default = 8,
                        help = "Proximity distance, in A.")
    parser.add_argument('outfile', type=str, help='the CSV output file')
    parser.add_argument('infiles', type=str, help='the XML validation file', nargs="+")
    args = parser.parse_args()
    
    # Recognize dash as stdout
    if args.outfile == "-":
        args.outfile = sys.stdout
    
    args.verbosity = sum(args.verbosity)
    
    return args

def guessCores(max = 16):
    """
    Try to guess how many cores are available.
    Returns the integer number of cores.
    
    :param max: maximum number of cores to auto-detect from python.
    This is important to not end up using all the cores of a very large
    shared machine.
    :type max: :int
    """
    # Slurm defines the environment variable SLURM_NTASKS
    slurm = os.getenv('SLURM_NTASKS')
    if slurm is not None:
        return int(slurm)
    
    # PBS and other schedulers may define $NCPUS
    ncpus = os.getenv('NCPUS')
    if ncpus is not None:
        return int(ncpus)
    
    # Fall back to the number given by python
    return min(multiprocessing.cpu_count(), max)
    
def processXMLFileAsync(infile, args):
    import traceback
    """
    Handles getting more than multiprocessing's minimal exception output
    """

    try:
        return processXMLFile(infile, args)
    except:
        # Put all exception text into an exception and raise that
        raise Exception(infile + " " + "".join(traceback.format_exception(*sys.exc_info())))

def processXMLFile(infile, args):
    start_time_entry = time.time()
    ost.LogScript(infile)
    
    # Compound lib should be initialized in the process, maybe?
    compound_lib = conop.CompoundLib.Load("/scicore/home/schwede/GROUP/OpenStructure/ChemLib/1.7/compounds.chemlib")
    conop.SetDefaultLib(compound_lib) # Set default again in worker threads
    
    try:
        doc = ValidationFactory(infile, compound_lib=compound_lib).getValidation()
    except AssertionError as e:
        return [infile], [{"reject_reason": str(e)}]
    except Exception as e:
        ost.LogError("Couldn't load validation data for %s" % infile)
        return [infile], [{"reject_reason": "Cannot load in OST"}]
    pdbid = doc.getPDBId()
    
    # Skip non X-Ray
    pdbx_method = doc.getMethod()
    if pdbx_method != 'X-RAY DIFFRACTION':
        ost.LogVerbose("Ignoring non x-ray structure: %s, %s" % (pdbid, pdbx_method))
        return [infile], [{"entry_method": pdbx_method}]

    try:
        xml = doc.getValidationXML()
        mapper = doc.getMapper()
        entry = xml.getEntry()
        resolution = entry.get("PDB-resolution")
        rfree = entry.get("PDB-Rfree")
        pdb_r = entry.get("PDB-R")
        clashscore = entry.get("clashscore")
        percent_rama_outliers = entry.get("percent-rama-outliers")
        attempted_validation_steps = entry.get("attemptedValidationSteps")
        attempted_mogul = "mogul" in attempted_validation_steps
        data_completeness = entry.get("DataCompleteness")
        percent_RSRZ_outliers = entry.get("percent-RSRZ-outliers")
        entry_atom_count = doc.countAtoms()
        entry_molprobity = doc.calcMolProbityOverallScore()
        entry_mean_b_factor = doc.calcMeanStructureBFactor()
        entry_median_b_factor = doc.calcMedianStructureBFactor()
    
        pdbx_resolution = doc.getResolution()
        try:
            pdbx_reflns_resolution = doc.getReflectionsResolution()
        except KeyError:
            pdbx_reflns_resolution = None
        meanI_over_sigI_obs = doc.getMeanIOverSigIObs()

        # Get info from the structure with OST
        mmcif_info = doc.getMMCIFInfo()
        mmcif_method = mmcif_info.GetMethod()
        mmcif_resolution = mmcif_info.GetResolution()

        # Ensure resolutions are within 0.1 in pdbx and ost.mmcifinfo
        #assert abs(pdbx_resolution - mmcif_resolution) < 0.1, "{0} != {1}".format(pdbx_resolution, mmcif_resolution)
        #assert float(resolution) == pdbx_resolution, "{0} != {1}".format(resolution, pdbx_resolution)
        assert pdbx_method == mmcif_method, "{0} != {1}".format(pdbx_method, mmcif_method)
    except AssertionError as e:
        ost.LogError("Ignoring structure breaking assertion: %s, %s" % (pdbid, str(e)))
        return [infile], [{"reject_reason": str(e)}]
    
    # Prepare lists to be returned
    subgroup_keys = []
    modelled_subgroup_dicts = [{}]


    # Generate a unique key
    subgroup_key = infile

    # Add Entry information
    modelled_subgroup_dicts[0]['entry_pdbid'] = pdbid
    modelled_subgroup_dicts[0]['entry_resolution'] = resolution
    modelled_subgroup_dicts[0]['entry_rfree'] = rfree
    modelled_subgroup_dicts[0]['entry_r'] = pdb_r
    modelled_subgroup_dicts[0]['entry_clashscore'] = clashscore
    modelled_subgroup_dicts[0]['entry_percent_rama_outliers'] = percent_rama_outliers
    modelled_subgroup_dicts[0]['entry_file'] =  infile
    modelled_subgroup_dicts[0]['entry_attempted_validation_steps'] =  attempted_validation_steps
    modelled_subgroup_dicts[0]['entry_attempted_mogul'] =  attempted_mogul
    modelled_subgroup_dicts[0]['entry_data_completeness'] =  data_completeness
    modelled_subgroup_dicts[0]['entry_percent_RSRZ_outliers'] =  percent_RSRZ_outliers
    modelled_subgroup_dicts[0]['entry_atom_count'] =  entry_atom_count
    modelled_subgroup_dicts[0]['entry_molprobity'] =  entry_molprobity
    modelled_subgroup_dicts[0]['entry_mean_b_factor'] =  entry_mean_b_factor
    modelled_subgroup_dicts[0]['entry_median_b_factor'] =  entry_median_b_factor


    modelled_subgroup_dicts[0]['entry_ost_resolution'] = mmcif_resolution
    modelled_subgroup_dicts[0]['entry_ost_method'] = mmcif_method
    modelled_subgroup_dicts[0]['entry_method'] = pdbx_method
    modelled_subgroup_dicts[0]['entry_ost_sloppy'] = doc.needOSTFaultTolerant()

    modelled_subgroup_dicts[0]['pdbx_resolution'] = pdbx_resolution
    modelled_subgroup_dicts[0]['pdbx_reflns_resolution'] = pdbx_reflns_resolution
    modelled_subgroup_dicts[0]['pdbx_meanI_over_sigI_obs'] = meanI_over_sigI_obs

    modelled_subgroup_dicts[0]['reject_reason'] = ""




    # return the result as key, value pair:
    subgroup_keys.append(subgroup_key)

    # Benchmark
    elapsed_time_entry = time.time() - start_time_entry
    for dict in modelled_subgroup_dicts:
        dict['entry_elapsed_time'] = elapsed_time_entry
    
    return subgroup_keys, modelled_subgroup_dicts

if __name__ == "__main__":
    """ Example usage:
    ./PDBValidation.py CameoValidation.csv ~cameo/evaluation/201[78]*/*/*validation.xml.gz
    """
    args = parse_args()
    infiles = args.infiles

    PushVerbosityLevel(args.verbosity)

    if len(infiles) == 1 and "*" in infiles[0]:
        infiles = glob.glob(infiles[0])
    
    if len(infiles) == 1:
        pairs = [processXMLFile(infiles[0], args)]
    else:
        # Sort the files to process last entries last, which are potentially larger
        infiles.sort(reverse=True)
        pool_size = min(len(infiles), args.ncores)
        pool = multiprocessing.Pool(pool_size)
        pairs = pool.map(partial(processXMLFileAsync, args=args), infiles)
        pool.close()
        pool.join()

    # Put everything in a dictionary
    ligand_dict = {}
    for keys, dicts in pairs:
        for key, value in zip(keys, dicts):
            if key is None and value is None:
                continue
            if key is None or value is None:
                raise KeyError("Key or value returned was None: %s, %s"%(key, value))   
            if key in ligand_dict:
                raise KeyError("Key already present in ligand_dict: %s"%key)
            ligand_dict[key] = value

    # Convert dict to Pandas
    df = pd.DataFrame.from_dict(ligand_dict, orient="index")
    df.to_csv(args.outfile)
