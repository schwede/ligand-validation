% Ligand Validation and Scoring

This project contains script for the validation of the quality of a PDB entry, ligand and binding site
based on the PDB Validation pipeline.

## Compile this README file
    pandoc README.md -s -o README.html
    pandoc README.md -t plain -o README


# Ligand Validation

## Usage

    PDBValidation [options] <mmCIF_file> <validation_file> <output_file>
    python PDBValidation.py [options] <output_file> <validation_file> [<validation_file>...]

## Installation

    cd </path/to/ligand-validation-project>
    python setup.py install
    # or:
    pip install .
    
    # Run tests:
    python setup.py test

## Introduction

The ligand validation script `PDBValidation` (`bin/PDBValidation`) validates the quality a PDB entry
and ligands and binding sites it includes, based on the wwPDB validation data.
It outputs a JSON file (or to STDOUT if <output_file> is "-") that
specifies whether the entry, ligand and proximity should be accepted or not. If the decision is to reject,
the reason for rejection is given too. An example output could be:

    {
        "entry": {"accept": true, "reason": ""},
        "ligands": {
            "C:ACT:.": {
                ".": {"accept": false, "reason": "RSCC < 0.9"}},
            "D:ACT:.": {
                ".": {"accept": true, "reason": "", "proximity":
                    {"accept": true, "reason": ""}, 
                }
            }
        }
    }

The PDB Validation script `PDBValidationExtract` is intended for data exploration purposes.
It takes a list of PDB Validation reports in gzipped XML format, extracts as much ligand validation information
as possible, and dumps the output in a CSV file, one ligand per line. Gzipped mmCIF files must be present in the same
folder as the validation files.

These two scripts are designed to be used in the context of ligand benchmarking and to establish a training set
of reliable ligands. 

## Changing filters

There are three ways to change filter options:

1. with command line arguments
2. with a configuration file
3. with a preset.

Filters can be repeated multiple times, overriding previous settings. This allows for instance to load a preset, 
followed by a configuration file and then additional command line arguments:

    LigandValidation.py -p cameo -c my_config.py --entry-resolution 3.0 ...

The last command line argument takes priority and the resolution threshold will be 3.0. Be careful to set the command
line arguments last, or they will be overridden by the preset and config. For instance the following call will filter
with a resolution of 2.2, as defined in the preset which is specified last:

    LigandValidation.py --entry-resolution 3.0 -p cameo ...

### Command line arguments
The filters are split in 3 main categories: '--entry-', '--ligand-' and '--proximity-'. They influence the filters
affecting entries, ligand and ligand proximity, respectively. For a full description of the filters and their
effects, see `LigandValidation.py --help` or the ["Options description"](#options-description) section below.

### Configuration files

The configuration file must be a valid Python 2.7 script which defines a 'config' dict with three keys: 'entry',
'ligand' and 'proximity'. The value of each of these keys is a dict with keys being the arguments minus the prefix.
Arguments don't have to be set if you want to keep the default values, including the top level keys.
For instance, to change the resolution threshold to 3.0 and RSCC to 0.95, and keep all other filters at their default
values, save a file named my_config.py with the following contents:

    config = {
        'entry': {
            'resolution': 3.0
            },
        'ligand': {
            'rscc': 0.95
        }
    }

and then call:

    LigandValidation.py -c my_config.py ...

### Presets

At the moment only one preset exits, 'cameo', with conservative filtering thresholds for ligand benchmarking.
Additional presets can be added to the 'PDBValidation/presets' folder. The format is identical to the configuration
files.

## Options Description

### Global Options

    -c config_file.py
    -p preset_name
Configuration file or preset name, see ["Changing filters"](#changing-filters) section above

    -i id
The format of ligand identifiers to report in the output. One of "mmcif", "auth" or "ost".

### Entry Options

Options controlling the filtering of the whole entry

    --entry-resolution ENTRY_RESOLUTION
Minimal resolution, in Å (inclusive).

    --entry-resolution-consistency ENTRY_RESOLUTION_CONSISTENCY
Maximal difference between the resolution before and after refinement
(mmCIF: _reflns.d_resolution_high and _refine.ls_d_res_high)

    --entry-percent-rama-outliers ENTRY_PERCENT_RAMA_OUTLIERS
Maximum percent-rama-outliers value allowed in PDB Validation report

    --entry-percent-RSRZ-outliers ENTRY_PERCENT_RSRZ_OUTLIERS
Maximum percent-RSRZ-outliers value allowed in PDB Validation report

    --entry-Rfree ENTRY_RFREE
Maximum Rfree value allowed in PDB Validation report

    --entry-R ENTRY_R
Maximum R value allowed in PDB Validation report

    --entry-median-B-factor ENTRY_MEDIAN_B_FACTOR
Maximum median B factor allowed in the entry. The median is calculated from all the atoms in the structure,
including waters and ligands.

    --entry-[no-]R-failed
Reject (no-: allow) entries where R values could not be reproduced by the PDB Validation report. This
is tested by testing if the report contains any "rsr" or "rscc" values in the <ModelledSubgroup>s. Structures
that contain even a single "rsr" or "rscc" values are considered to have succeeded R values reproduction,
even if some or most <ModelledSubgroup>s are missing.

    --entry-[no-]OST-fault-tolerant
Reject (no-: allow) entries that had to be read with `fault_tolerant=True` by OpenStructure.
This is the case of a few structures with duplicated atoms or similar issues.


### Ligand Options

    --ligand-rscc LIGAND_RSCC
Minimum RSCC (Real Space Correlation Coefficient between the calculated and experimental model maps).
Value between -1 and 1, inclusive.
    
    --ligand-B LIGAND_B
    --ligand-B-quantile LIGAND_B_QUANTILE
Maximum B factor allowed at the quantile LIGAND_B_QUANTILE of all atoms of the ligand. If LIGAND_B_QUANTILE
is 1 this corresponds to the maximum value, meaning that no single atom of the ligand can exceed the value
of LIGAND_B
 
    --ligand-neighborhood-B-factor-multiplier LIGAND_NEIGHBORHOOD_B_FACTOR_MULTIPLIER
This filter looks at the neighborhood (PROXIMITY_DISTANCE, in Å) of each atom of the ligand in turn.
It calculates the median B factor of all atoms within PROXIMITY_DISTANCE Å of the current atom,
excluding the current atom, but including other atoms of the ligand.
If the current atom is more than LIGAND_NEIGHBORHOOD_B_FACTOR_MULTIPLIER times higher than the median of the 
neighborhood, the ligand is rejected. The procedure is repeated on each atom of the ligand.

    --ligand-structure-B-factor-multiplier LIGAND_STRUCTURE_B_FACTOR_MULTIPLIER
Ligand that contain any atom with a B factor more than LIGAND_STRUCTURE_B_FACTOR_MULTIPLIER times higher
than the median B factor of all atoms in the entry (as calculated for the `--entry-median-B-factor` filter)
are rejected.
    
    --ligand-[no-]null-occupancy
Reject (no-: allow) ligands that have at least one atom with 0 occupancy.

    --ligand-[no-]ost-alternative
Reject (no-: allow) ligands that OST cannot read due to alternative configuration atoms. This has 
no effect if alternative configurations are rejected (see `--ligand-[no-]alternative-configurations`).
 
    --ligand-[no-]inconsistent-atom-count
Reject (no-: allow) ligands where the counts of heavy atoms in OST, the PDBX mmCIF reader and the OST
Compound Lib differ. This is typically the case when incomplete residues are present in the structure,
but also when OST fails to read partial alternative configurations (ligand would be rejected by 
`--ligand-[no-]alternative-configurations` anyway), or if the structure contains duplicated atoms
which are counted twice by the PDBX mmCIF reader (`--entry-[no-]OST-fault-tolerant` would reject the complete
structure).

    --ligand-[no-]unknown-atoms
Reject (no-: allow) ligands containing unknown atoms (X).

    --ligand-[no-]alternative-configurations
Reject (no-: allow) ligands that have atoms with alternative configurations.

    --ligand-[no-]clashing-partial-occupancy
Reject (no-: allow) ligand where an atom has a clash reported in the PDB Validation report and has an occupancy < 1.
This is to catch cases such as A:BR.402 and A:BR.403 in PDB entry 6D0G or B:CL.1330 and B:CL.1332 in 5G6U, where
the ligands are likely to exist in separate molecules and should be annotated as alternative configurations.ands

    --ligand-[no-]geometry-outlier
    --ligand-[no-]density-outlier
    --ligand-[no-]chirality-outlier
    --ligand-[no-]clashes-outlier
Reject (no-: allow) ligands that are marked as X-outlier in the PDB Validation report.


### Proximity Options

Proximity options are identical to the [Ligand Options](#ligand-options), except they operate
on the residue (possibly ligand residues) that have atoms within PROXIMITY_DISTANCE of the ligand.
Whole residues are considered, even atoms possibly outside the distance.

    --proximity-distance PROXIMITY_DISTANCE
Radius of the proximity, in Å.                         
    
    --proximity-rscc PROXIMITY_RSCC
    --proximity-rsrz PROXIMITY_RSRZ
    --proximity-B PROXIMITY_B
    --proximity-B-quantile PROXIMITY_B_QUANTILE
    --proximity-[no-]null-occupancy
    --proximity-[no-]ost-alternative
    --proximity-[no-]inconsistent-atom-count
    --proximity-[no-]unknown-atoms
    --proximity-[no-]alternative-configurations
    --proximity-[no-]clashing-partial-occupancy
    --proximity-[no-]geometry-outlier
    --proximity-[no-]density-outlier
    --proximity-[no-]chirality-outlier
    --proximity-[no-]clashes-outlier
See [Ligand Options](#ligand-options).


## Requirements

- Python 3
- lxml
- pandas
- py-mmcif (mmCIF Core Access Library)
- OpenStructure

The lxml, pandas and mmcif dependencies will be automatically installed upon installation with pip.
The OpenStructure package isn't available through PyPI

### Compound Library
A working compound library is required. If it is not know by OpenStructure (which it should if OST was
properly installed), we will first search for a "compounds.chemlib" file in OpenStructure's shared folder,
then in the folder the COMPLIB_PATH environment variable points to. If no compound lib can be found,
a RuntimeError is raised.

## Examples

See the LigandValidation.sh script for examples.

## Tests

Automated unit tests can be run with the `run_tests.sh` script. Individual test suites can be run with:

    python -m unittest PDBValidation.tests.test_Mapping
    python -m unittest PDBValidation.tests.test_PDBXReader
    python -m unittest PDBValidation.tests.test_Residue
    python -m unittest PDBValidation.tests.test_Validation
    python -m unittest PDBValidation.tests.test_XML
