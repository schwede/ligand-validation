# LigandValidation.sh configuration
# This configuration file will be executed as a python script.
# It must create a 'config' dictionary variable with three entries:
# entry, ligand and proximity.

config = {
    'entry': {
        'resolution': 2.2,
        'resolution-consistency': 0.1,
        'percent-rama-outliers': 2.0,
        'percent-RSRZ-outliers': 20.0,
        'Rfree': 0.25,
        'R': 0.3,
        'R-failed': True,
        'OST-fault-tolerant': True,
        'median-B-factor': 120
        },
    'ligand': {
        'rscc': 0.9,
        'B': 120,
        'B-quantile': 1.0,
        'neighborhood-B-factor-multiplier': 2.5,
        'structure-B-factor-multiplier': 3.0,
        'alternative-configurations': True,
        'clashing-partial-occupancy': True,
        'geometry-outlier': True,
        'density-outlier': True,
        'chirality-outlier': True,
        'clashes-outlier': True,
        'null-occupancy': True,
        'ost-alternative': True,
        'inconsistent-atom-count': True,
        'unknown-atoms': True
        },
    'proximity': {
        'distance': 8.0,
        'rsrz': 2.0,
        'rscc': 0.9,
        'B': 120,
        'B-quantile': 1.0,
        'geometry-outlier': True,
        'density-outlier': True,
        'chirality-outlier': True,
        'clashes-outlier': True,
        'null-occupancy': True,
        'ost-alternative': True,
        'inconsistent-atom-count': True,
        'unknown-atoms': True,
        'alternative-configurations': True,
        'clashing-partial-occupancy': True
        }
    }
