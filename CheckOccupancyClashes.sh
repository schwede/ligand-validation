#!/bin/bash

#SBATCH --job-name=CheckOccupancyClashes-cameo-17-18
#SBATCH --time=24:00:00
#SBATCH --mem=64G
#SBATCH --qos=1day
#SBATCH -N 1
#SBATCH -n 16
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=xavier.robin@unibas.ch


. ~/activate-ost-develop
. ~/python/lig_bench_venv/bin/activate

./CheckOccupancyClashes.py -n 32 CAMEO_evaluation/CameoOccupancyClashes.csv ~cameo/evaluation/201[78]*/*/*validation.xml.gz
#./PDBValidationMP.py CameoValidationMP.csv ~cameo/evaluation/2018-01*/*/*validation.xml.gz

