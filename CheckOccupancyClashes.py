#!/usr/bin/env python3

import argparse
from functools import partial
import multiprocessing
import os
import sys
import time

import pandas as pd

from ost import GetVerbosityLevel, PushVerbosityLevel
from ost import conop
import ost

from PDBValidation.Mapping import *
from PDBValidation.PDBXReader import *
from PDBValidation.Residue import *
from PDBValidation.Validation import *
from PDBValidation.XML import *
from PDBValidation.utils import *

def parse_args():
    parser = argparse.ArgumentParser(description='Process a PDB Validation XML file.')
    parser.add_argument('-v', '--verbose', dest='verbosity',  action='append_const',
                        const=1, default = [2],
                        help = "Increase verbosity. Can be used multiple times.")
    parser.add_argument('-q', '--quiet', dest='verbosity',  action='append_const',
                        const = -1,
                        help = "Decrease verbosity. Can be used multiple times.")
    parser.add_argument('-n', '--ncores', default = guessCores(), type = int,
                        help = "Number of parallel processes to use.")
    parser.add_argument('-d', '--distance', default = 8,
                        help = "Proximity distance, in A.")
    parser.add_argument('outfile', type=str, help='the CSV output file')
    parser.add_argument('infiles', type=str, help='the XML validation file', nargs="+")
    args = parser.parse_args()
    
    # Recognize dash as stdout
    if args.outfile == "-":
        args.outfile = sys.stdout
    
    args.verbosity = sum(args.verbosity)
    
    return args

def guessCores(max = 16):
    """
    Try to guess how many cores are available.
    Returns the integer number of cores.
    
    :param max: maximum number of cores to auto-detect from python.
    This is important to not end up using all the cores of a very large
    shared machine.
    :type max: :int
    """
    # Slurm defines the environment variable SLURM_NTASKS
    slurm = os.getenv('SLURM_NTASKS')
    if slurm is not None:
        return int(slurm)
    
    # PBS and other schedulers may define $NCPUS
    ncpus = os.getenv('NCPUS')
    if ncpus is not None:
        return int(ncpus)
    
    # Fall back to the number given by python
    return min(multiprocessing.cpu_count(), max)
    
def processXMLFileAsync(infile, args):
    import traceback
    """
    Handles getting more than multiprocessing's minimal exception output
    """

    try:
        return processXMLFile(infile, args)
    except:
        # Put all exception text into an exception and raise that
        raise Exception(infile + " " + "".join(traceback.format_exception(*sys.exc_info())))

def processXMLFile(infile, args):
    start_time_entry = time.time()
    ost.LogScript(infile)
    
    # Compound lib should be initialized in the process, maybe?
    compound_lib = conop.CompoundLib.Load("/scicore/home/schwede/GROUP/OpenStructure/ChemLib/1.7/compounds.chemlib")
    conop.SetDefaultLib(compound_lib) # Set default again in worker threads
    
    try:
        doc = PDBValidation.CreateFromValidation(infile, compound_lib)
    except AssertionError as e:
        return [], []
    pdbid = doc.getPDBId()
    
    # Skip non X-Ray
    pdbx_method = doc.getMethod()
    if pdbx_method != 'X-RAY DIFFRACTION':
        ost.LogVerbose("Ignoring non x-ray structure: %s, %s" % (pdbid, pdbx_method))
        return [], []
    
    xml = doc.getValidationXML()
    mapper = doc.getMapper()
    entry = xml.getEntry()
    clashscore = entry.get("clashscore")

    # Prepare lists to be returned
    subgroup_keys = []
    modelled_subgroup_dicts = []
    
    for ligand_with_alt in doc.findNonPolymerLigands():
        # Store occupancies and clashes per atom
        occupancies = {}
        clashes = {}
        symm_clashes = {}
        if isinstance(ligand_with_alt, ModelledSubgroupNotFound):
            ost.LogWarning("ModelledSubgroupNotFound: ignoring ligand " + ligand_with_alt.pdbid + " " + ligand_with_alt.xpath)
            continue

        ligand_alt_configs = ligand_with_alt.listAlt()
        for ligand_alt_config in ligand_alt_configs:
            start_time_ligand = time.time()
            ligand = ligand_with_alt.getAlt(ligand_alt_config)
            modelled_subgroup = ligand.modelled_subgroup
            
            # Get the ModelledSubgroup as a dict:
            modelled_subgroup_dict = elementToDictRecursive(modelled_subgroup)
            chain = modelled_subgroup_dict['missing_XML_subgroup'] = False
            chain = modelled_subgroup_dict['ModelledSubgroup.chain']
            res_name = modelled_subgroup_dict['ModelledSubgroup.resname']
            res_num = modelled_subgroup_dict['ModelledSubgroup.resnum']
            ins_code = modelled_subgroup_dict['ModelledSubgroup.icode']
            model = modelled_subgroup_dict['ModelledSubgroup.model']
            altcode = modelled_subgroup_dict['ModelledSubgroup.altcode']
            entity = modelled_subgroup_dict['ModelledSubgroup.ent']
            assert model == "1", "Unhandled model number: '%s'"%model

            # Store occupancies per atom
            for atom_name, occupancy in zip(ligand.getAtomNames(), ligand.getOccupancies()):
                occupancies.setdefault(atom_name, 0)
                occupancies[atom_name] += occupancy

            for clash in modelled_subgroup.findall("clash"):
                clashes[clash.get('atom')] = elementToDict(clash)
            for clash in modelled_subgroup.findall("symm-clash"):
                symm_clashes[clash.get('atom')] = elementToDict(clash)

            # Generate a unique key
        subgroup_key = createLigandAuthIDWithPDBID(pdbid, chain, res_name, res_num, ins_code, altcode)

        clash_list_O = list()
        clash_list_atom = list()
        symm_clash_list_O = list()
        symm_clash_list_atom = list()
        for atom, value in clashes.items():
            try:
                occ = occupancies[atom]
                if occ != 1 and occ != 0:
                    clash_list_atom.append(atom)
                    clash_list_O.append(occ)
            except KeyError as e:
                if not atom.startswith("H"):
                    raise e
        for atom, value in symm_clashes.items():
            try:
                occ = occupancies[atom]
                if occ != 1 and occ != 0:
                    symm_clash_list_atom.append(atom)
                    symm_clash_list_O.append(occ)
            except KeyError as e:
                if not atom.startswith("H"):
                    raise e

        # Add Entry information
        modelled_subgroup_dict['entry_pdbid'] = pdbid
        modelled_subgroup_dict['entry_clashscore'] = clashscore
        modelled_subgroup_dict['entry_file'] =  infile
        modelled_subgroup_dict['ligand_clash_atoms'] = ";".join(clash_list_atom)
        modelled_subgroup_dict['ligand_clash_O'] = ";".join([str(o) for o in clash_list_O])
        modelled_subgroup_dict['ligand_symm_clash_atoms'] = ";".join(symm_clash_list_atom)
        modelled_subgroup_dict['ligand_symm_clash_O'] = ";".join([str(o) for o in symm_clash_list_O])

        # return the result as key, value pair:
        subgroup_keys.append(subgroup_key)
        modelled_subgroup_dict['ligand_elapsed_time'] = time.time() - start_time_ligand
        modelled_subgroup_dicts.append(modelled_subgroup_dict)
    
    # Benchmark
    elapsed_time_entry = time.time() - start_time_entry
    for dict in modelled_subgroup_dicts:
        dict['entry_elapsed_time'] = elapsed_time_entry
    
    return subgroup_keys, modelled_subgroup_dicts

if __name__ == "__main__":
    """ Example usage:
    ./CheckOccupancyClashes.py CameoValidation.csv ~cameo/evaluation/201[78]*/*/*validation.xml.gz
    """
    args = parse_args()
    infiles = args.infiles
    if "/scicore/home/schwede/cameo/evaluation/2018-01-06T05-00/2018-01-06_00000154/6em0_validation.xml.gz" in infiles:
        infiles.remove("/scicore/home/schwede/cameo/evaluation/2018-01-06T05-00/2018-01-06_00000154/6em0_validation.xml.gz")
        sys.stderr.write("Skipping 6em0 due to invalid mmCIF file\n")

    PushVerbosityLevel(args.verbosity)
    
    if len(infiles) == 1:
        pairs = [processXMLFile(infiles[0], args)]
    else:
        pool_size = min(len(infiles), args.ncores)
        pool = multiprocessing.Pool(pool_size)
        pairs = pool.map(partial(processXMLFileAsync, args=args), infiles)
        pool.close()
        pool.join()

    # Put everything in a dictionary
    ligand_dict = {}
    for keys, dicts in pairs:
        for key, value in zip(keys, dicts):
            if key is None and value is None:
                continue
            if key is None or value is None:
                raise KeyError("Key or value returned was None: %s, %s"%(key, value))
            if key in ligand_dict:
                raise KeyError("Key already present in ligand_dict: %s"%key)
            ligand_dict[key] = value

    # Convert dict to Pandas
    df = pd.DataFrame.from_dict(ligand_dict, orient="index")
    df.to_csv(args.outfile)
