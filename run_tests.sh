#!/bin/sh
python -m unittest discover

# Slower variant disabled. In addition we would have to keep
# this list up to date or risk missing new tests.
# python -m unittest PDBValidation.tests.test_Mapping
# python -m unittest PDBValidation.tests.test_PDBXReader
# python -m unittest PDBValidation.tests.test_Residue
# python -m unittest PDBValidation.tests.test_Validation
# python -m unittest PDBValidation.tests.test_XML
