import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pdb-validation",
    version="0.0.1",
    author="Xavier Robin",
    author_email="xavier.robin@unibas.ch",
    description="Validate entries and ligands with the PDB Validation report",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.scicore.unibas.ch/schwede/ligand-validation/",
    #packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: ???",
        "Operating System :: OS Independent",
    ],
    install_requires=['lxml', 'pandas', 'mmcif'],
    test_suite="PDBValidation.tests",
    packages=['PDBValidation', 'PDBValidation/presets'],
    scripts=['bin/PDBValidation', 'bin/PDBValidationExtract'],
)
