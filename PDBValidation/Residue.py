import warnings
import ost

from . import Validation
from .XML import *

class AtomCountError(ValueError):
    pass

class Residue():
    """ A view of a Residue that integrates validation, PDBX and OST annotations"""

    def __init__(self, modelled_subgroup, pdbx_residue, ost_residue, conop_compound):
        """ Constructor
        
        :param modelled_subgroup: the <ModelledSubgroup> element. Not :class`ModelledSubgroups`!
        :type modelled_subgroup: :class:`lxml.Element`
        :param pdbx_residue: the PDBXAtomSites instance containing the atoms (atom.site) information.
        No checking is made about correctness.
        :type pdbx_residue: `PDBXAtomSites`
        :param ost_residue: the OST ResidueHandle
        :type ost_residue: :class:`ost.mol.ResidueHandle` or :class:`ost.mol.ResidueView`
        :param conop_compound: the Compound Library Compound
        :type conop_compound: :class:`ost.conop.Compound`
        """
        # Make sure Residue is created from a single alternative configuration
        altcodes_pdbx = pdbx_residue.listAlt()
        if len(altcodes_pdbx) > 1 and '.' in altcodes_pdbx:
            altcodes_pdbx.remove(".")
        if len(altcodes_pdbx) > 1:
            raise ValueError("Residue can only be created from a single alternative configuration")
        self.pdbx_altcode = list(altcodes_pdbx)[0]

        if isinstance(modelled_subgroup, ModelledSubgroups):
            raise ValueError("Passed a ModelledSubgroups object instead of a single <ModelledSubgroup> element")

        self._assertIdenticalAlts(modelled_subgroup.get('altcode'), self.pdbx_altcode)

        self.pdbx_residue = pdbx_residue
        self.ost = ost_residue
        self.modelled_subgroup = modelled_subgroup
        self.conop = conop_compound

        # Allow None in conop only for unknown residues
        if conop_compound is None and self.getResName() not in Validation.PDBValidation.unknown_resnames:
            raise ValueError("Missing compound in compound library is not unknown: %s" % self.getResName())
        # Ensure ost Residue is valid
        if not ost_residue.IsValid():
            raise ValueError("Invalid OST residue")
        # Ensure residue names match
        #if pdbx_residue.getResName() != ost_residue.name or ost_residue.name != conop_compound.three_letter_code:
        if pdbx_residue.getResName() != "UNL" and pdbx_residue.getResName() != conop_compound.three_letter_code:
            raise ValueError("Inconsistent residue names")

    def __str__(self):
        return "{cls} {chain}:{res_name}.{res_num}-{altcode}".format(
            cls = self.__class__.__name__,
            chain = self.getMmCIFChain(),
            res_name = self.getResName(),
            res_num = self.getMmCIFResNum(),
            altcode = self.getAltCode()
            )

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def __eq__(self, other):
        """Equality comparison: is it the same residue, based on chain, residue and atom names and positions.
        Note: may be tricked if the residues come from a different PDB entry, so make sure to check PDB entry
        separately if relevant."""
        if isinstance(self, other.__class__):
            return self.pdbx_residue == other.pdbx_residue # Look only at pdbx_residue which has an __eq__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def getMmCIFID(self):
        return createLigandMmCIFID(self.getMmCIFChain(), self.getResName(), self.getMmCIFResNum(), self.getAltCode())

    def getMachineMmCIFID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'."""
        return createLigandMachineMmCIFID(self.getMmCIFChain(), self.getResName(), self.getMmCIFResNum(), self.getAltCode(), sep=sep)

    def getOSTID(self):
        return createLigandMmCIFID(self.getMmCIFChain(), self.getResName(), self.getOSTResNum(), self.getAltCode())

    def getMachineOSTID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'."""
        return createLigandMachineMmCIFID(self.getMmCIFChain(), self.getResName(), self.getOSTResNum(), self.getAltCode(), sep=sep)

    def getAuthID(self):
        return createLigandAuthID(self.getAuthChain(), self.getResName(), self.getAuthResNum(), self.getInsertionCode(),
                              self.getAltCode())

    def getMachineAuthID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'."""
        return createLigandMachineAuthID(self.getAuthChain(), self.getResName(), self.getAuthResNum(), self.getInsertionCode(),
                              self.getAltCode(), sep=sep)

    def countAtomsConop(self):
        if self.conop is None:
            return None
        return len([atom for atom in self.conop.atom_specs])

    def countAtomsHeavyConop(self):
        if self.conop is None:
            return None
        return len([atom for atom in self.conop.atom_specs if atom.element != 'H'])

    def countAtomsOST(self):
        return self.ost.atom_count

    def countAtomsHeavyOST(self):
        return len([atom for atom in self.ost.atoms if atom.element != 'H'])

    def countAtomsPDBX(self):
        return self.pdbx_residue.shape[0]

    def countAtomsHeavyPDBX(self):
        return (self.pdbx_residue['type_symbol'] != 'H').sum()

    def isAtomCountConsistent(self):
        """ Checks if the number of heavy atoms is consistent.
        PDBX and OST should match exactly.
        The compound library is allowed to be off by one to allow for residues
        in polymer chain. However no checking is actually done that the 
        residue is indeed in a polymer chain in the base class
        """
        conop_compound_heavy_count = self.countAtomsHeavyConop()
        pdbx_heavy_count = self.countAtomsHeavyPDBX()
        ost_heavy_count = self.countAtomsHeavyOST()
        if (conop_compound_heavy_count != pdbx_heavy_count and
            conop_compound_heavy_count -1 != pdbx_heavy_count) or \
            (conop_compound_heavy_count != ost_heavy_count and
            conop_compound_heavy_count - 1 != ost_heavy_count):
            return False
        return True

    def _assertConsistentAtomCount(self):
        if not self.isAtomCountConsistent():
            raise AtomCountError("Inconsistent atom count for {chain}:{resname}.{resnum}: {cconop}, {cpdbx}, {cost}".format(
                chain = self.pdbx_residue.getMmCIFChain(),
                resnum = self.pdbx_residue.getMmCIFResNum(),
                resname = self.pdbx_residue.getResName(),
                cconop = self.countAtomsHeavyConop(),
                cpdbx = self.countAtomsHeavyPDBX(),
                cost = self.countAtomsHeavyOST()))

    def countAtoms(self):
        """ Count atoms. Makes sure the count is consistent
        and raises an AtomCountError otherwise"""
        self._assertConsistentAtomCount()
        return self.countAtomsConop()

    def countAtomsHeavy(self):
        """ Count heavy atoms. Makes sure the count is consistent
        and raises an AtomCountError otherwise"""
        self._assertConsistentAtomCount()
        return self.countAtomsHeavyConop()

    def countUnknownAtoms(self):
        """ Count unknown atoms. Only uses the PDBX information.
        Makes sure the count is consistent and raises an
        AtomCountError otherwise"""
        return sum(self.pdbx_residue['type_symbol'] == 'X')

    def getOccupancies(self):
        return self.pdbx_residue.occupancy

    def getAtomNames(self):
        return self.pdbx_residue.label_atom_id

    def getBFactors(self):
        return self.pdbx_residue.B_iso_or_equiv

    def countO_lt_0_9(self):
        return sum(self.getOccupancies() < 0.9)

    def countNullOccupancies(self):
        return any(self.getOccupancies() <= 0)

    def getType(self):
        """ Test the type of instance: AminoAcid, Ligand, DNA, RNA or Ignored.
        raises an error when tested on an instance of the base Residue class."""
        raise RuntimeError("Base class Residue %s has no type" % self)

    def isType(self, what):
        """ Test if the instance is of a specific type: AminoAcid, Ligand, DNA, RNA or Ignored.
        raises an error when tested on an instance of the base Residue class."""
        raise RuntimeError("Base class Residue %s has no type" % self)

    def getChemClass(self):
        """ Compound Library Chem Class (str) """
        if self.conop is None:
            return None
        return self.conop.chem_class

    def getChemType(self):
        """ Compound Library Chem Type (str) """
        if self.conop is None:
            return None
        return self.conop.chem_type

    def getResName(self):
        """ Residue name """
        return self.pdbx_residue.getResName()

    def getEntity(self):
        """ Entity """
        return self.pdbx_residue.getEntity()

    def getMmCIFChain(self):
        """ Chain (mmCIF name) """
        return self.pdbx_residue.getMmCIFChain()

    def getAuthChain(self):
        """ Chain (author name) """
        return self.pdbx_residue.getAuthChain()

    def getMmCIFResNum(self):
        """ Residue number (mmCIF position) """
        return self.pdbx_residue.getMmCIFResNum()

    def getOSTResNum(self):
        """ Residue number (OST position) """
        return self.ost.number

    def getAuthResNum(self):
        """ Residue number (Author position) """
        return self.pdbx_residue.getAuthResNum()

    def getInsertionCode(self):
        """ Insertion code (Author position) """
        return self.pdbx_residue.getInsertionCode()

    def getAltCode(self):
        """ Alternative configuration code, pdbx format (ie dot notation) """
        return self.pdbx_altcode

    def getRSCC(self):
        try:
            return float(self.modelled_subgroup.get("rscc"))
        except (TypeError, ValueError):
            return float("NaN")

    def getRSR(self):
        try:
            return float(self.modelled_subgroup.get("rsr"))
        except (TypeError, ValueError):
            return float("NaN")

    def getRSRZ(self):
        try:
            return float(self.modelled_subgroup.get("rsrz"))
        except (TypeError, ValueError):
            return float("NaN")

    def isOutlier(self, category):
        if category not in {"geometry", "density", "chirality", "clashes"}:
            raise ValueError("Unknown category %s" % category)
        attribute = "ligand_{category}_outlier".format(category=category)
        return self.modelled_subgroup.get(attribute) == "yes"

    def isHet(self):
        return self.ost.IsLigand();

    @staticmethod
    def _assertIdenticalAlts(modelled_subgroups_alt, pdbx_alt):
        if modelled_subgroups_alt == " ":
            modelled_subgroups_alt = "."
        assert modelled_subgroups_alt == pdbx_alt, "%s != %s" % (modelled_subgroups_alt, pdbx_alt)

    @classmethod
    def _CreateWithAlt(cls, doc, pdbx_residue, ost_residue, comp_lib,
                       entity, chain, res_num, ins_code, altcode):

        if altcode is None:
            modelled_subgroups = doc.xml.getModelledSubgroups(entity, chain, res_num, 1,
                                                              icode = ins_code)
            create_class = cls._detectResidueClass(doc, modelled_subgroups, pdbx_residue, ost_residue, comp_lib)
            return ResidueAlternativeConfigurations(modelled_subgroups, pdbx_residue,
                                                    ost_residue, comp_lib, create_class)
        else:
            modelled_subgroup = doc.xml.getModelledSubgroup(entity, chain, res_num, 1,
                                                            icode = ins_code, altcode = altcode)
            create_class = cls._detectResidueClass(doc, modelled_subgroup, pdbx_residue, ost_residue, comp_lib)
            pdbx_residue_alt = pdbx_residue.getAlt(altcode)
            conop_compound = comp_lib.FindCompound(pdbx_residue_alt.getResName())
            return create_class(doc, modelled_subgroup, pdbx_residue_alt, ost_residue, conop_compound)

    @classmethod
    def CreateFromMmCIFPosition(cls, chain, res_num, entity, doc, altcode = None):
        """ Create Residue from an mmCIF chain name, residue number and entity.
        
        :param chain: chain name
        :type chain: str
        :param res_num: residue number
        :type res_num: int
        :param entity: entity number
        :type entity: :str or :int
        :param doc: the PDBValidation object of the current structure
        :type doc: :class:`PDBValidation`
        :param altcode: optional alternative configuration code.
        If None, a list of Residues will be returned
        :type altcode: :str
        
        :return: an object inheriting from Residue if altcode was supplied.
        Otherwise a list of such objects, possibly with a single element.
        """
        pdbx_residue = doc.getPdbxResidueMmCIFPosition(chain, res_num, entity)
        author_chain = pdbx_residue.getAuthChain()
        author_res_num = pdbx_residue.getAuthResNum()
        author_ins_code = pdbx_residue.getInsertionCode()

        ost_residue = doc.getEntity().FindChain(chain).FindResidue(1 if res_num == '.' else int(res_num))
        comp_lib = doc.getCompoundLib()

        return cls._CreateWithAlt(doc, pdbx_residue, ost_residue, comp_lib,
                                   entity, author_chain, author_res_num, author_ins_code,
                                   altcode)

    @classmethod
    def CreateFromAuthorPosition(cls, chain, res_num, ins_code, doc, altcode = None):
        """ Create Residue from an author PDB chain name, residue number and insertion code.
        
        :param chain: chain name
        :type chain: str
        :param res_num: residue number
        :type res_num: int
        :param ins_code: insertion code
        :type ins_code: str
        :param doc: the PDBValidation object of the current structure
        :type doc: :class:`PDBValidation`
        :param altcode: optional alternative configuration code.
        If None, a list of Residues will be returned
        :type altcode: :str
        """
        pdbx_residue = doc.getPdbxResidueAuthorPosition(chain, res_num, ins_code)
        entity = pdbx_residue.getEntity()
        mmcif_chain = pdbx_residue.getMmCIFChain()
        mmcif_res_num = pdbx_residue.getMmCIFResNum()

        ost_residue = doc.getEntity().FindChain(mmcif_chain).FindResidue(1 if mmcif_res_num == '.' else int(mmcif_res_num))
        comp_lib = doc.getCompoundLib()

        return cls._CreateWithAlt(doc, pdbx_residue, ost_residue, comp_lib,
                                   entity, chain, res_num, ins_code,
                                   altcode)

    @classmethod
    def _detectResidueClass(cls, doc, modelled_subgroup, pdbx_residue, ost_residue, comp_lib):
        if doc.isNonPolymerLigand(pdbx_residue.getEntity(), pdbx_residue.getResName()):
            return Ligand
        elif ost_residue.peptide_linking:
            return AminoAcid
        elif ost_residue.chem_class == 'R':
            return RNA
        elif ost_residue.chem_class == 'S':
            return DNA
        elif ost_residue.name in Validation.PDBValidation.ignore_resnames:
            return Ignored
        else:
            return PolymerChainResidue
#            ost.LogError(pdbx_residue)
#            raise ValueError("Cannot guess the type of residue")

    @classmethod
    def CreateLigandOrAminoAcid(cls, doc, modelled_subgroup, pdbx_residue, ost_residue, comp_lib):
        """ Create Ligand or AminoAcid Residue, automatically choosing the correct class.
        
        :param doc: the PDBValidation object of the current structure
        :type doc: :class:`PDBValidation`
        :param modelled_subgroup: the <ModelledSubgroup> element. Not :class`ModelledSubgroups`!
        :type modelled_subgroup: :class:`lxml.Element`
        :param pdbx_residue: the PDBXAtomSites instance containing the atoms (atom.site) information.
        No checking is made about correctness.
        :type pdbx_residue: `PDBXAtomSites`
        :param ost_residue: the OST ResidueHandle
        :type ost_residue: :class:`ost.mol.ResidueHandle` or :class:`ost.mol.ResidueView`
        :param comp_lib: the Compound Library
        :type comp_lib: :class:`ost.comp_lib.CompoundLib`
        
        Raises ValueError if the type of residue cannot be guessed.
        """
        return _detectResidueClass(doc, modelled_subgroup, pdbx_residue,
                                   ost_residue, comp_lib)(
                                       modelled_subgroup, pdbx_residue,
                                       ost_residue, comp_lib
                                       )

    @classmethod
    def coordsEqual(cls, ost, pdbx):
        """ Test equality of coordinates.
        :param ost: a Vec3 position from ost (ost.mol.AtomHandle.GetPos())
        :type ost: :class:`ost.geom.Vec3`
        :param pdbx: a row from the PDBX Atom Sites with the Cartn_* coordinates
        :type pdbx: :class:`pandas.Series`.
        
        :return: :bool
         """
        return cls.coordEqual(ost.x, pdbx['Cartn_x']) and \
               cls.coordEqual(ost.y, pdbx['Cartn_y']) and \
               cls.coordEqual(ost.z, pdbx['Cartn_z'])

    @classmethod
    def coordEqual(cls, ost, pdbx, tol = 0.0005):
        """ Test equality of 1 coordinate (x, y or z) between pdbx and ost
        :param ost: the OST position
        :type ost: :float
        :param pdbx: the PDBX position
        :type pdbx: :str
        :param tol: the tolerance over the distance. Should match the precision
        of the mmCIF file
        :type tol: float
        
        :return: bool
        """
        return abs(ost - float(pdbx)) <= tol

    def checkCoords(self):
        """ Checks the consistency of PDBX and OST coordinates.
        Normally OST should read the first alternative configuration. Check if that's really
        the case, and return True or False accordingly"""
        ost_residue = self.ost
        for index, row in self.pdbx_residue.iterrows():
            atom_name = row['label_atom_id']
            ost_atom = ost_residue.FindAtom(atom_name)
            if ost_atom is None:
                print("Atom not found in OST: " + row['type_symbol'] + "-" + row['label_atom_id'])
                raise RuntimeError("Atom not found: %s:%s" % (str(self), atom_name))
                return False
            # Check coordinates
            ost_pos = ost_atom.GetPos()
            if not self.coordsEqual(ost_pos, row):
                ost.LogTrace("x: %s == %s: %s" % (ost_pos.x, row['Cartn_x'], self.coordEqual(ost_pos.x, row['Cartn_x'])))
                ost.LogTrace("y: %s == %s: %s" % (ost_pos.y, row['Cartn_y'], self.coordEqual(ost_pos.y, row['Cartn_y'])))
                ost.LogTrace("z: %s == %s: %s" % (ost_pos.z, row['Cartn_z'], self.coordEqual(ost_pos.z, row['Cartn_z'])))
                ost.LogDebug("Coords differ: %s:%s" % (str(self), atom_name))
                return False
            # Check occupancy
            if abs(ost_atom.GetOccupancy() - float(row['occupancy'])) > 0.001:
                ost.LogTrace("Occupancy diff: %s - %s = %s" % (ost_atom.GetOccupancy(), row['occupancy'], ost_atom.GetOccupancy() - float(row['occupancy'])))
                ost.LogDebug("Occupancies differ: %s:%s" % (str(self), atom_name))
                return False
            # Check B factor
            if abs(ost_atom.GetBFactor() - float(row['B_iso_or_equiv'])) > 0.001:
                ost.LogTrace("B Factor diff: %s - %s = %s" % (ost_atom.GetBFactor(), row['B_iso_or_equiv'], ost_atom.GetBFactor() - float(row['B_iso_or_equiv'])))
                ost.LogDebug("B Factors differ: %s:%s" % (str(self), atom_name))
                return False

        return True

    def getOSTProximity(self, distance, doc):
        """ Returns an ost.Mol.EntityView of the atoms within 'distance' A of the Residue.

        :param distance: radius, in Angstroems
        :type distance: :float
        :param doc: the PDBValidation object
        :type doc: :class:`PDBValidation`
        """
        query_string = "({distance} <> [cname='{chain}' and rnum={res_num} and rname='{res_name}']) " \
                       " and not (cname=='{chain}' and rnum=={res_num} and rname=='{res_name}')".format(
                distance=distance, chain=self.getMmCIFChain(), res_num=self.getOSTResNum(),
                res_name = self.getResName())
        ost.LogDebug("Proximity query: %s" % query_string)
        return doc.getEntity().Select(query_string)

    def findNeighborhoodByAtom(self, distance, doc):
        """ Return a generator of tuples containing the atom and its neighborhood.
        The generator yields a tuple (ost.mol.Atom, :list(:float)) per atom in the Residue.

        :param distance: radius, in Angstroems
        :type distance: :float
        :param doc: the PDBValidation object
        :type doc: :class:`PDBValidation`
        """
        for atom in self.ost.atoms:
            query_string = "({distance} <> [aindex={aindex}]) " \
                           " and not (aindex=={aindex})".format(
                distance=distance, aindex=atom.index)
            ost.LogDebug("Atom proximity query: %s" % query_string)
            yield (atom, doc.getEntity().Select(query_string))

    def findNeighborhoodBFactors(self, distance, doc):
        """ Return a generator of the B factors of the atoms in the proximity of the Residue.
        The generator yields a tuple (ost.mol.Atom, :list(:float)) per atom in the Residue.
        This is not the same as findProximityResidues() and then getBFactors, as that would count the atoms
        of all the residues that have any atom in the proximity, including atoms beyond distance."""
        for atom, nh in self.findNeighborhoodByAtom(distance, doc):
            yield (atom, [atom.b_factor for atom in nh.atoms])

    def findClashingAtoms(self):
        """ Find clashes reported in the PDB Validation XML for this ligand. Return a generator that yields the
        atoms of the ligand that are clashing, as a tuple of (atom name (:str), ost.mol.AtomView). They might be invalid handles, especially
        in the case of H atoms."""
        for atom_name in self.modelled_subgroup.getClashAtomNames():
            ost_atom = self.ost.FindAtom(atom_name)
            yield (ost_atom, atom_name)

    def hasClashingPartialOccupancyAtoms(self):
        """ Tests if the residue has atom with clashes reported in the PDB Validation report and partial occupancies.
        This typically suggests that atoms cannot exist together in the same molecule and should therefore
        be labeled as alternative configurations. However for some reason that wasn't done."""
        for atom, atom_name in self.findClashingAtoms():
            if not atom.IsValid():
                if atom_name.startswith("H"):
                    pass
                else:
                    raise RuntimeError("Invalid atom %s" % atom_name)
            elif atom.occupancy < 1:
                return True
        return False

class Ligand(Residue):
    """ A view of a Ligand that integrates validation, PDBX and OST annotations."""
    type = ["Ligand"]

    def getLigRSRZ(self):
        return self.modelled_subgroup.get("ligRSRZ")

    def isType(self, what):
        return what.lower in [x.lower for x in self.type]

    def getType(self):
        return self.type

    def findProximityResidues(self, distance, doc, ignore_water = True,
                              ignore_missing_subgroups = False):
        """ Returns a generator of `Residue`s within `distance`
        
        :param distance: radius, in Angstroems
        :type distance: :float
        :param doc: the PDBValidation object
        :type doc: :class:`PDBValidation`
        :param ignore_water: whether to ignore waters. False is not supported yet
        due to the fact they are not numbered in the CIF file and can't be reliably
        fetched.
        :type ignore_water: :bool
        :param ignore_missing_subgroups: ignore proximity residues that have no
        <ModelledSubgroup> entry. If True, the XML.ModelledSubgroupNotFound error
        is caught and the residue will be missing from the returned list.
        :type ignore_missing_subgroups: :bool
        """
        query_string = "({distance} <> [cname='{chain}' and rnum={res_num} and rname='{res_name}']) " \
                       " and not (cname=='{chain}' and rnum=={res_num} and rname=='{res_name}')".format(
                distance=distance, chain=self.getMmCIFChain(), res_num=self.getOSTResNum(),
                res_name = self.getResName())
        ost.LogDebug("Proximity query: %s" % query_string)
        ost_proximity = doc.getEntity().Select(query_string, ost.mol.QueryFlag.MATCH_RESIDUES)

        if len(ost_proximity.chains) == 0:
            warnings.warn("No proximity found for ligand %s!" % str(self))

        for prox_chain in ost_proximity.chains:
            for prox_residue in prox_chain.residues:
                if prox_residue.name == "HOH" and ignore_water:
                    continue
                ost.LogDebug("Found proximity residue: %s.%s.%s" % (prox_chain.name,
                                                                    prox_residue.name,
                                                                    prox_residue.number))
                # mmCIF should have no ins_code, but make sur of that before taking ResNum.num
                assert prox_residue.number.ins_code == '\0'
                try:
                    yield Residue.CreateFromMmCIFPosition(prox_chain.name, prox_residue.number.num, None, doc)
                except ModelledSubgroupNotFound as e:
                    if ignore_missing_subgroups:
                        ost.LogWarning("Missing proximity residue %s. Remember to flag the ligand." % str(e))
                    else:
                        yield e

    def isAtomCountConsistent(self):
        """ Check if the number of heavy atoms is consistent.
        PDBX, OST and the compound library should match exactly.
        """
        conop_compound_heavy_count = self.countAtomsHeavyConop()
        pdbx_heavy_count = self.countAtomsHeavyPDBX()
        ost_heavy_count = self.countAtomsHeavyOST()
        if conop_compound_heavy_count != pdbx_heavy_count or \
            conop_compound_heavy_count != ost_heavy_count:
            return False
        return True

    @classmethod
    def _detectResidueClass(cls, doc, modelled_subgroup, pdbx_residue, ost_residue, conop_compound):
        return cls # return Ligand

class PolymerChainResidue(Residue):
    type = ["PolymerChainResidue"]

    def isType(self, what):
        return what.lower == "polymerchainresidue"

    def getType(self):
        return self.type

    def isAtomCountConsistent(self):
        """ Check if the number of heavy atoms is consistent.
        PDBX and OST should match exactly.
        The compound library is allowed to be off by one to allow for residues
        in polymer chains. If so we check that the missing atom is flagged with
        is_leaving = True. However no chemical check is performed.
        """
        conop_compound_heavy_count = self.countAtomsHeavyConop()
        pdbx_heavy_count = self.countAtomsHeavyPDBX()
        ost_heavy_count = self.countAtomsHeavyOST()
        if (conop_compound_heavy_count != pdbx_heavy_count and
            conop_compound_heavy_count -1 != pdbx_heavy_count) or \
            (conop_compound_heavy_count != ost_heavy_count and
            conop_compound_heavy_count - 1 != ost_heavy_count):
            # Maybe the missing atom is_leaving
            residue_atom_names = [atom.name for atom in self.ost.atoms]
            for atom in self.conop.atom_specs:
                if atom.name not in residue_atom_names:
                    if atom.is_leaving:
                        return True
                    else:
                        return False
            assert False, "Unreachable code"
        return True

    @classmethod
    def _detectResidueClass(cls, doc, modelled_subgroup, pdbx_residue, ost_residue, conop_compound):
        return cls # This will return the derived class in the derived classes.
                   # ie. AminoAcid._detectResidueClass() returns AminoAcid, not PolymerChainResidue
                   # even if the function is defined here.
                   # This means there is no need to re-define it in the derived classes


class AminoAcid(PolymerChainResidue):
    """ A view of an Amino Acid that integrates validation, PDBX and OST annotations."""
    type = PolymerChainResidue.type + ["AminoAcid"]

    def __init__(self, modelled_subgroup, pdbx_atom_sites, ost_residue, conop_compound):
        if not ost_residue.chem_class.IsPeptideLinking():
            raise ValueError("Not an amino acid: {0}!".format(ost_residue.name))
        Residue.__init__(self, modelled_subgroup, pdbx_atom_sites, ost_residue, conop_compound)

    def isType(self, what):
        return what.lower in [x.lower for x in self.type]

    def getType(self):
        return self.type


class DNA(PolymerChainResidue):
    """ A view of a DNA residue."""
    type = PolymerChainResidue.type + ["DNA"]

    def isType(self, what):
        return what.lower in [x.lower for x in self.type]

    def getType(self):
        return self.type


class RNA(PolymerChainResidue):
    """ A view of an RNA residue."""
    type = PolymerChainResidue.type + ["RNA"]

    def isType(self, what):
        return what.lower in [x.lower for x in self.type]

    def getType(self):
        return self.type


class Ignored(Residue):
    """ A view of an Ignored residue."""
    type = ["Ignored"]

    def isType(self, what):
        return what.lower in [x.lower for x in self.type]

    def getType(self):
        return self.type

    @classmethod
    def _detectResidueClass(cls, doc, modelled_subgroup, pdbx_residue, ost_residue, conop_compound):
        return cls

class ResidueAlternativeConfigurations():
    """ Stores the alternative configurations of a residue.
    Provides the hasAlt, listAlt and getAlt functions to access 
    a specific configuration.
    
    Because it is creating new instances of the alternative residues, it needs to store
    the class of the alternative Residues (in self.create_class).
     """

    def __init__(self, modelled_subgroups, pdbx_residues, ost_residue, comp_lib, create_class):
        self.modelled_subgroups = modelled_subgroups
        self.pdbx_residues = pdbx_residues
        self.ost_residue = ost_residue
        self.comp_lib = comp_lib
        self.create_class = create_class # Class to use to create residues upon getAlt()
        alt_configs = pdbx_residues.listAlt()
        if len(alt_configs) > 1 and '.' in alt_configs:
            alt_configs.remove('.')
        self.alt_configs = alt_configs

    def getAlt(self, alt_code = None):
        """ Returns the Residue of the given alternative configuration.
        Atoms with no alternative configuration are denoted with a space in the XML file.
        None, a dot or an empty string will be converted to such a space.
        
        The return type is a ModelledSubgroup object, which inherits from etree.Element.
        """
        modelled_subgroups_alts = self.modelled_subgroups.listAlt()
        pdbx_alts = self.pdbx_residues.listAlt()

        if alt_code == '.' or alt_code is None or alt_code == '' or alt_code == " ":
            alt_code_ms = " "
            alt_code_pdbx = "."
        else:
            alt_code_ms = alt_code
            alt_code_pdbx = alt_code

        if not alt_code_pdbx in pdbx_alts:
            raise KeyError(alt_code)

        modelled_subgroup = self.modelled_subgroups.getAlt(alt_code_ms)
        pdbx_residue = self.pdbx_residues.getAlt(alt_code_pdbx)
        conop_compound = self.comp_lib.FindCompound(pdbx_residue.getResName())

        return self.create_class(modelled_subgroup,
                                 pdbx_residue,
                       self.ost_residue, conop_compound)

    def hasAlt(self):
        """ Tests if this residue has alternative configurations. """
        return len(self.alt_configs) > 1

    def listAlt(self):
        """ Lists all alternative configurations."""
        return self.alt_configs

    def iter(self):
        for alt in self.listAlt():
            yield alt, self.getAlt(alt)

    def getMmCIFID(self):
        """ Get a machine-readable ligand ID. Without Alt code."""
        return createLigandMmCIFID(self.pdbx_residues.getMmCIFChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getMmCIFResNum())

    def getMachineMmCIFID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code."""
        return createLigandMachineMmCIFID(self.pdbx_residues.getMmCIFChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getMmCIFResNum(), sep=sep)

    def getOSTID(self):
        """ Get a machine-readable ligand ID. Without Alt code."""
        return createLigandMmCIFID(self.pdbx_residues.getMmCIFChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getOSTResNum())

    def getMachineOSTID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code."""
        return createLigandMachineMmCIFID(self.pdbx_residues.getMmCIFChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getOSTResNum(), sep=sep)

    def getAuthID(self):
        """ Get a machine-readable ligand ID. Without Alt code."""
        return createLigandAuthID(self.pdbx_residues.getAuthChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getAuthResNum(), self.pdbx_residues.getInsertionCode())

    def getMachineAuthID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code."""
        return createLigandMachineAuthID(self.pdbx_residues.getAuthChain(), self.pdbx_residues.getResName(),
                              self.pdbx_residues.getAuthResNum(), self.pdbx_residues.getInsertionCode(),
                              sep=sep)

