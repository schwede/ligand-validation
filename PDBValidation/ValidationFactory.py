import os
import re

from ost import io, conop, GetSharedDataPath
from ost.io import LoadMMCIF

from .Mapping import PDBMapper
from .Validation import PDBValidation
from .PDBXReader import PDBX
from .XML import PDBValidationXML


class ValidationFactory():
    def __init__(self, validation_path, mmcif_path = None, compound_lib = None):
        if mmcif_path is None:
            mmcif_path = self.getMmCIFPathFromValidation(validation_path)

        self.xml = PDBValidationXML(validation_path)
        self._loadMmCifOst(mmcif_path) # sets self.ent, self.ost_fault_tolerant, self.seqres, self.mmcif_info
        self.pdbx = PDBX.CreateFromGZIP(mmcif_path)
        self.atom_sites = self.pdbx.getAtomSites()
        self._loadReflsShell() # sets self.reflns_shell
        self._loadNonPolymerLigands() # sets self.non_polymer_ligands
        self.mapper = PDBMapper(self.atom_sites)

        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
        self.compound_lib = compound_lib

        # Check PDB IDs
        pdbid_xml = self.xml.getEntry().get("pdbid") # From validation XML
        pdbid_cif = self.pdbx.getObj("entry").getValue("id")
        assert pdbid_xml == pdbid_cif, \
            "XML PDB ID differ from mmCIF: %s, %s" % (pdbid_xml, pdbid_cif)
        self.pdbid = pdbid_xml

    def _loadMmCifOst(self, mmcif_path):
        """ Loads the structure with OST. Sets self.ent, self.ost_fault_tolerant, self.seqres, self.mmcif_info """
        self.ost_fault_tolerant = False
        try:
            self.ent, self.seqres, self.mmcif_info = LoadMMCIF(mmcif_path, fault_tolerant=False, seqres = True, info = True)
        except Exception:
            self.ost_fault_tolerant = True
            self.ent, self.seqres, self.mmcif_info = LoadMMCIF(mmcif_path, fault_tolerant=True, seqres = True, info = True)

    def _loadReflsShell(self):
        try:
            self.reflns_shell = self.pdbx.getObjAsDF("reflns_shell")
        except KeyError:
            self.reflns_shell = False

    def _loadNonPolymerLigands(self):
        if self.pdbx.exists("pdbx_entity_nonpoly"):
            self.non_polymer_ligands = self.pdbx.getObjAsDF("pdbx_entity_nonpoly")
        else:
            self.non_polymer_ligands = None

    @staticmethod
    def getMmCIFPathFromValidation(xmlfile):
        """ CIF file must be either in the same folder, or an environment variable PDB_VALIDATION_PATH must be present,
        indicating the full path of a PDB validation XML file with placeholders for the "short" (2-letter) and "full"
        pdb IDs. For instance:
        export PDB_VALIDATION_PATH="/scicore/data/managed/PDB/latest/validation_reports/{short}/{full}/{full}_validation.xml.gz"
        """
        # Same folder?
        ciffile = xmlfile.replace("_validation.xml", ".cif")
        if not os.path.isfile(ciffile):
            # PDBID
            pdbid_search = re.search('([a-z0-9]{4})_validation\.xml(.gz)?', xmlfile, re.IGNORECASE)
            if pdbid_search:
                pdbid = pdbid_search.group(1)
                pdb_short = pdbid[1:3]
                pdb_cif_path = os.getenv("PDB_CIF_PATH")
                if not pdb_cif_path:
                    raise ValueError("Cannot find CIF file for validation file %s" % xmlfile)
                ciffile = pdb_cif_path.format(short=pdb_short, full=pdbid)
                if not os.path.isfile(ciffile):
                    raise ValueError("Cannot find CIF file for validation file %s" % xmlfile)
            else:
                raise ValueError("Cannot extract PDB ID from %s" % xmlfile)
        return ciffile

    def getValidation(self):
        return PDBValidation(self.xml, self.ent, self.ost_fault_tolerant, self.seqres, self.mmcif_info, self.pdbx,
                             self.atom_sites, self.reflns_shell, self.non_polymer_ligands, self.mapper,
                             self.compound_lib, self.pdbid)
