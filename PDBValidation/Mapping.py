import numpy as np
import ost

class PDBMapper():
    """ A class to map author and mmcif chain-residue coordinates"""
    
    def __init__(self, atom_sites):
        self.atom_sites = atom_sites
        
    def mmcifToAuthor(self, chain, resnum = None):
        """ Converts new mmCIF coordinates to old PDB author ones
        Specifically:
        chain: label_asym_id -> auth_asym_id
        resnum: label_seq_id -> auth_seq_id, pdbx_PDB_ins_code (if resnum is provided only)
        extra: a pd.DataFrame with extra information (not mapped) about the chain: pdbx_PDB_model_num;
        and residue, if resnum was given: inscode and altcode.
        """
        if resnum is None:
            # Chain only
            resultset = self.atom_sites.query('label_asym_id == "{0}"'.format(chain))
            # Make sure we get a single entity and model number
            assert len(resultset['label_entity_id'].unique()) == 1
            assert len(resultset['pdbx_PDB_model_num'].unique()) == 1

            extra = resultset[["auth_asym_id", "pdbx_PDB_model_num"]].drop_duplicates()
            assert len(resultset["auth_asym_id"].unique()) == 1
            return resultset["auth_asym_id"].unique()[0], extra
        else:
            # Chain + residue
            resultset = self.atom_sites.query('(label_asym_id == "{0}") and (label_seq_id == "{1}")'.format(chain, resnum))
            # Check comp_id identity
            if (resultset['label_comp_id'] != resultset['auth_comp_id']).any():
                ost.LogError(resultset)
                raise RuntimeError("label_comp_id != auth_comp_id")
            
            extra = resultset[["auth_asym_id", "auth_seq_id", "auth_comp_id", "label_entity_id", "pdbx_PDB_model_num",
                                "pdbx_PDB_ins_code", "label_alt_id"]].drop_duplicates()
            result = extra[["auth_asym_id", "auth_seq_id", "pdbx_PDB_ins_code"]].drop_duplicates()
            if result.shape[0] != 1:
                # When a ligand is its own chain, it will have resnum = '.' in the CIF file.
                # OST labels it as 1, so retry with . instead:
                if resnum == 1:
                    ost.LogInfo("Retry with resnum = . instead of 1")
                    return self.mmcifToAuthor(chain, '.')
                ost.LogError(chain, resnum)
                ost.LogError(result)
                raise RuntimeError("deduplicated result contained multiple or no rows")
            
            # Make sure we get a single entity, model and insertion code
            assert len(resultset['label_entity_id'].unique()) == 1
            assert len(resultset['pdbx_PDB_model_num'].unique()) == 1
            assert len(resultset['pdbx_PDB_ins_code'].unique()) == 1
            
            # Missing icode will be "?". convert to " " maybe?
            icode = result.iloc[0]["pdbx_PDB_ins_code"]
            if icode == "?":
                icode = None
            
            return result.iloc[0]["auth_asym_id"], result.iloc[0]["auth_seq_id"], \
                   icode, extra
    
        
    def authorToMmCIF(self, chain, resnum = None, icode = None):
        """ Converts old PDB author coordinates to new mmCIF ones
        Specifically:
        chain: auth_asym_id -> label_asym_id (list)
        resnum: auth_seq_id -> label_seq_id
        extra: a pd.DataFrame with extra information (not mapped) about the chain: label_entity_id", "pdbx_PDB_model_num;
        and residue, if resnum was given: label_alt_id.
        """
        if resnum is None and icode is None:
            # Chain only
            resultset = self.atom_sites.query('auth_asym_id == "{0}"'.format(chain))
            # Make sure we get a single model number. Entity may be split (ligands)
            assert len(resultset['pdbx_PDB_model_num'].unique()) == 1
            
            extra = resultset[["label_asym_id", "label_entity_id", "pdbx_PDB_model_num"]].drop_duplicates()
            return resultset["label_asym_id"].unique()[0], extra
   
        elif resnum is not None:
            if icode is None or icode == " " or icode == "":
                icode = "?"
            # Chain + residue
            resultset = self.atom_sites.query('(auth_asym_id == "{0}") and (auth_seq_id == "{1}") ' \
                                              ' and (pdbx_PDB_ins_code == "{2}")'.format(chain, resnum, icode))
            # Check identity of comp_id (residue name), entity, model
            if (resultset['auth_comp_id'] != resultset['label_comp_id']).any():
                ost.LogError(resultset)
                raise RuntimeError("auth_comp_id != label_comp_id")
            # Check unicity of entity and model
            assert len(resultset['label_entity_id'].unique()) == 1
            assert len(resultset['pdbx_PDB_model_num'].unique()) == 1
            
            extra = resultset[["label_asym_id", "label_seq_id", "label_alt_id",
                               "label_entity_id", "label_comp_id", "pdbx_PDB_model_num"]].drop_duplicates()
            result = extra[["label_asym_id", "label_seq_id"]].drop_duplicates()
            # Check we get a unique result
            if (result.shape[0] != 1):
                ost.LogError(chain, resnum)
                ost.LogError(result)
                raise RuntimeError("deduplicated result contained multiple or no rows")
            
            return result.iloc[0]["label_asym_id"], result.iloc[0]["label_seq_id"], extra
        else:
            raise ValueError("resnum must be given with icode.")


class BioUnitMapper(PDBMapper):
    """ A class to map author and mmcif chain-residue coordinates"""

    def __init__(self, atom_sites, biounit):
        self.atom_sites = atom_sites
        self.biounit = biounit
        # We don't deal with peptide ligands/polymer ligands yet. This can be relaxed in the future...
        assert np.all(np.array([r.number.ins_code for r in biounit.FindChain("_").residues]) == '\0'), \
            "Insertion code detected in _ chain (potential peptide ligand)"
        # We assume that there is no insertion code at all in mmCIF and therefore in the biounit
        assert np.all(np.array([r.number.ins_code for r in biounit.Select("cname!='-'").residues]) == '\0'), \
            "Insertion code detected"

    def buToMmCIF(self, chain, resnum = None):
        if resnum is None:
            # Chain only
            if chain == "_" or chain == "-":
                raise ValueError("Cannot map biounit ligand and water chains back to a single mmCIF chain.")
            mmcif_chain = self.biounit.FindChain(chain).GetStringProp("original_name")
            resultset = self.atom_sites.query('auth_asym_id == "{0}"'.format(mmcif_chain))
            # Make sure we get a single model number. Entity may be split (ligands)
            assert len(resultset['pdbx_PDB_model_num'].unique()) == 1

            extra = resultset[["label_asym_id", "label_entity_id", "pdbx_PDB_model_num"]].drop_duplicates()
            return resultset["label_asym_id"].unique()[0], extra

        else:
            residue = self.biounit.FindResidue("_", resnum)
            mmcif_chain = residue.GetStringProp("original_name")
            if chain == "_":
                mmcif_resnum = "."
            elif chain == "-":
                raise ValueError("Mapping of water not supported")
            else:
                mmcif_resnum = resnum

            resultset = self.atom_sites.query(
                '(label_asym_id == "{0}") and (label_seq_id == "{1}")'.format(mmcif_chain, mmcif_resnum))
            # Check comp_id matches
            if not (resultset['label_comp_id'] == residue.name).any():
                ost.LogError(resultset)
                raise RuntimeError("label_comp_id != residue.name")

            extra = resultset[["label_asym_id", "label_seq_id", "label_alt_id",
                               "label_entity_id", "label_comp_id", "pdbx_PDB_model_num"]].drop_duplicates()
            result = extra[["label_asym_id", "label_seq_id"]].drop_duplicates()
            # Check we get a unique result
            if (result.shape[0] != 1):
                ost.LogError(chain, resnum)
                ost.LogError(result)
                raise RuntimeError("deduplicated result contained multiple or no rows")

            return result.iloc[0]["label_asym_id"], result.iloc[0]["label_seq_id"], extra
