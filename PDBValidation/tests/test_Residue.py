import math
import os
import unittest

import numpy

from ost import io, conop, GetSharedDataPath

from ..Validation import *
from ..ValidationFactory import ValidationFactory
from ..Residue import *

class TestResidue(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5h6c_validation_fn = os.path.join(cls.data_dir, "5h6c_validation.xml.gz")
        cls.pdbx_5h6c_cif_fn = os.path.join(cls.data_dir, "5h6c.cif.gz")
        cls.pdbx_5lh4_validation_fn = os.path.join(cls.data_dir, "5lh4_validation.xml.gz")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")
        cls.pdbx_5x1g_validation_fn = os.path.join(cls.data_dir, "5x1g_validation.xml.gz")
        cls.pdbx_5x1g_cif_fn = os.path.join(cls.data_dir, "5x1g.cif.gz")
        cls.pdbx_5n7l_validation_fn = os.path.join(cls.data_dir, "5n7l_validation.xml.gz")
        cls.pdbx_5n7l_cif_fn = os.path.join(cls.data_dir, "5n7l.cif.gz")
        cls.pdbx_6d0g_validation_fn = os.path.join(cls.data_dir, "6d0g_validation.xml.gz")
        cls.pdbx_6d0g_cif_fn = os.path.join(cls.data_dir, "6d0g.cif.gz")
        cls.pdbx_6ess_validation_fn = os.path.join(cls.data_dir, "6ess_validation.xml.gz")
        cls.pdbx_6ess_cif_fn = os.path.join(cls.data_dir, "6ess.cif.gz")
        # Get some residues
        doc_5h6c = ValidationFactory(cls.pdbx_5h6c_validation_fn, cls.pdbx_5h6c_cif_fn).getValidation()
        cls.doc_5h6c = doc_5h6c
        xml_5h6c = doc_5h6c.getValidationXML()
        conop = doc_5h6c.getCompoundLib()
        cls.conop = conop
        cls.res_5h6c_A_GAI202 = {
            'ms': xml_5h6c.getModelledSubgroups(2, "A", 202, 1, " "),
            'pdbx': doc_5h6c.getPdbxResidueAuthorPosition("A", "202", None),
            'ost': doc_5h6c.getEntity().FindChain("C").FindResidue(1),
            'conop':  conop.FindCompound("GAI")
        }
        
        doc_5lh4 = ValidationFactory(cls.pdbx_5lh4_validation_fn, cls.pdbx_5lh4_cif_fn).getValidation()
        cls.doc_5lh4 = doc_5lh4
        xml_5lh4 = doc_5lh4.getValidationXML()
        conop = doc_5lh4.getCompoundLib()
        cls.res_5lh4_A_GLY188 = {
            'ms': xml_5lh4.getModelledSubgroups(1, "A", 188, 1, " "),
            'pdbx': doc_5lh4.getPdbxResidueAuthorPosition("A", "188", None),
            'ost': doc_5lh4.getEntity().FindChain("A").FindResidue(169),
            'conop':  conop.FindCompound("GLY")
        }
        cls.res_5lh4_A_LYS188A = {
            'ms': xml_5lh4.getModelledSubgroups(1, "A", 188, 1, "A"),
            'pdbx': doc_5lh4.getPdbxResidueAuthorPosition("A", "188", "A"),
            'ost': doc_5lh4.getEntity().FindChain("A").FindResidue(170),
            'conop':  conop.FindCompound("LYS")
        }
        cls.res_5lh4_A_6W4 = {
            'ms': xml_5lh4.getModelledSubgroups(5, "A", 305, 1, " "),
            'pdbx': doc_5lh4.getPdbxResidueMmCIFPosition("F", ".", "5"),
            'ost': doc_5lh4.getEntity().FindChain("F").FindResidue(1),
            'conop':  conop.FindCompound("6W4")
        }
        cls.res_5lh4_A_ASN223 = {
            'ms': xml_5lh4.getModelledSubgroups(1, "A", 245, 1, " "),
            'pdbx': doc_5lh4.getPdbxResidueMmCIFPosition("A", "223", "1"),
            'ost': doc_5lh4.getEntity().FindChain("A").FindResidue(223),
            'conop':  conop.FindCompound("ASN")
        }
        doc_5x1g = ValidationFactory(cls.pdbx_5x1g_validation_fn, cls.pdbx_5x1g_cif_fn).getValidation()
        cls.doc_5x1g = doc_5x1g
        xml_5x1g = doc_5x1g.getValidationXML()
        cls.res_5x1g_A_SER32 = {
            'ms': xml_5x1g.getModelledSubgroups(1, "C", 544, 1, " "),
            'pdbx': doc_5x1g.getPdbxResidueMmCIFPosition("A", "32", "1"),
            'ost': doc_5x1g.getEntity().FindChain("A").FindResidue(32),
            'conop':  conop.FindCompound("SER")
        }
        cls.res_5x1g_C_ARG535 = {
            'ms': xml_5x1g.getModelledSubgroups(1, "C", 535, 1, " "),
            'pdbx': doc_5x1g.getPdbxResidueMmCIFPosition("A", "23", "1"),
            'ost': doc_5x1g.getEntity().FindChain("A").FindResidue(23),
            'conop':  conop.FindCompound("ARG")
        }
        doc_5n7l = ValidationFactory(cls.pdbx_5n7l_validation_fn, cls.pdbx_5n7l_cif_fn).getValidation()
        cls.doc_5n7l = doc_5n7l
        xml_5n7l = doc_5n7l.getValidationXML()
        cls.res_5n7l_F_GLU = {
            'ms': xml_5n7l.getModelledSubgroups(3, "A", 405, 1, " "),
            'pdbx': doc_5n7l.getPdbxResidueMmCIFPosition("F", ".", "3"),
            'ost': doc_5n7l.getEntity().FindChain("F").FindResidue(1),
            'conop':  conop.FindCompound("GLU")
        }
        cls.res_5n7l_A_GLY80 = {
            'ms': xml_5n7l.getModelledSubgroups(1, "A", 382, 1, " "),
            'pdbx': doc_5n7l.getPdbxResidueMmCIFPosition("A", "80", "1"),
            'ost': doc_5n7l.getEntity().FindChain("A").FindResidue(80),
            'conop':  conop.FindCompound("GLY")
        }

        doc_6ess = ValidationFactory(cls.pdbx_6ess_validation_fn, cls.pdbx_6ess_cif_fn).getValidation()
        cls.doc_6ess = doc_6ess
        xml_6ess = doc_6ess.getValidationXML()
        cls.res_6ess_A_4IR400 = {
            'ms': xml_6ess.getModelledSubgroups(2, "A", 400, 1, " "),
            'pdbx': doc_6ess.getPdbxResidueMmCIFPosition("B", ".", None),
            'ost': doc_6ess.getEntity().FindChain("B").FindResidue(1),
            'conop':  conop.FindCompound("4IR")
        }
        
        doc_6d0g = ValidationFactory(cls.pdbx_6d0g_validation_fn, cls.pdbx_6d0g_cif_fn).getValidation()
        cls.doc_6d0g = doc_6d0g
        xml_6d0g = doc_6d0g.getValidationXML()
        cls.res_6d0g_A_BR402 = {
            'ms': xml_6d0g.getModelledSubgroups(3, "A", 402, 1, " "),
            'pdbx': doc_6d0g.getPdbxResidueMmCIFPosition("C", ".", None),
            'ost': doc_6d0g.getEntity().FindChain("C").FindResidue(1),
            'conop':  conop.FindCompound("BR")
        }
        
    def test__init__Base(self):
        """ Can instantiate Residues with Base class"""
        Residue(self.res_5lh4_A_GLY188['ms'].getAlt(None),
                self.res_5lh4_A_GLY188['pdbx'].getAlt(None),
                self.res_5lh4_A_GLY188['ost'],
                self.res_5lh4_A_GLY188['conop'])
        Residue(self.res_5lh4_A_LYS188A['ms'].getAlt("A"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("A"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        Residue(self.res_5lh4_A_LYS188A['ms'].getAlt("B"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("B"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        Residue(self.res_5lh4_A_6W4['ms'].getAlt("A"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("A"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        Residue(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])

    def test__init__Derived(self):
        """ Can instantiate Residues with derived classes"""
        AminoAcid(self.res_5lh4_A_GLY188['ms'].getAlt(None),
                self.res_5lh4_A_GLY188['pdbx'].getAlt(None),
                self.res_5lh4_A_GLY188['ost'],
                self.res_5lh4_A_GLY188['conop'])
        AminoAcid(self.res_5lh4_A_LYS188A['ms'].getAlt("A"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("A"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        AminoAcid(self.res_5lh4_A_LYS188A['ms'].getAlt("B"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("B"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        Ligand(self.res_5lh4_A_6W4['ms'].getAlt("A"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("A"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        Ligand(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])

    def test__init__Invalid(self):
        """ Cannot instantiate inconsistent stuff """
        with self.assertRaises(ValueError):
            Ligand(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                   self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                   self.res_5lh4_A_6W4['ost'],
                   self.conop.FindCompound("ASP")) # Wrong conop compound
        with self.assertRaises(ValueError):
            Ligand(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                   self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                   self.doc_5lh4.getEntity().FindChain("F").FindResidue(100), # Wrong Residue
                   self.res_5lh4_A_6W4['conop'])

    def test_Ligand(self):
        """ Ligand data looks all right"""
        lig = Ligand(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        self.assertEqual(str(lig), "Ligand F:6W4..-B")
        self.assertEqual(lig.countAtomsConop(), 74)
        self.assertEqual(lig.countAtomsHeavyConop(), 40)
        self.assertEqual(lig.countAtomsOST(), 40) # Hydrogens not in structure
        self.assertEqual(lig.countAtomsHeavyOST(), 40)
        self.assertEqual(lig.countAtomsPDBX(), 40)
        self.assertEqual(lig.countAtomsHeavyPDBX(), 40)
        self.assertEqual(lig.countAtoms(), 74)
        self.assertEqual(lig.countAtomsHeavy(), 40)
        self.assertEqual(lig.countUnknownAtoms(), 0)
        self.assertTrue(lig.isAtomCountConsistent())
        self.assertFalse(lig.checkCoords())
        self.assertEqual(lig.getOccupancies().tolist()[1], 1)
        self.assertEqual(lig.getOccupancies().tolist()[39], 0.5)
        self.assertEqual(lig.getBFactors().tolist()[1], 16.85)
        self.assertEqual(lig.getBFactors().tolist()[39], 16.36)
        self.assertEqual(lig.countO_lt_0_9(), 1)
        self.assertEqual(lig.countNullOccupancies(), 0)
        self.assertEqual(lig.getType(), ['Ligand'])
        self.assertTrue(lig.isType("Ligand"))
        self.assertEqual(lig.getChemClass(), "N")
        self.assertEqual(lig.getChemType(), "M")
        self.assertEqual(lig.getResName(), "6W4")
        self.assertEqual(lig.getEntity(), "5")
        self.assertEqual(lig.getMmCIFChain(), "F")
        self.assertEqual(lig.getAuthChain(), "A")
        self.assertEqual(lig.getMmCIFResNum(), ".")
        self.assertEqual(lig.getOSTResNum(), 1)
        self.assertEqual(lig.getAuthResNum(), "305")
        self.assertEqual(lig.getInsertionCode(), "?")
        self.assertEqual(lig.getAltCode(), "B")
        self.assertAlmostEqual(lig.getRSCC(), 0.959)
        self.assertAlmostEqual(lig.getRSR(), 0.075)
        self.assertTrue(math.isnan(lig.getRSRZ()))
        self.assertEqual(lig.getAuthID(), "A:6W4.305-B")
        self.assertEqual(lig.getMachineAuthID(), "A:6W4:305::B")
        self.assertEqual(lig.getMmCIFID(), "F:6W4..-B")
        self.assertEqual(lig.getMachineMmCIFID(), "F:6W4:.:B")
        self.assertEqual(lig.getOSTID(), "F:6W4.1-B")
        self.assertEqual(lig.getMachineOSTID(), "F:6W4:1:B")

    def test_LigandA(self):
        """ Ligand data looks all right"""
        lig = Ligand(self.res_5lh4_A_6W4['ms'].getAlt("A"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("A"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        self.assertEqual(lig.countAtomsConop(), 74)
        self.assertEqual(lig.countAtomsHeavyConop(), 40)
        self.assertEqual(lig.countAtomsOST(), 40) # Hydrogens not in structure
        self.assertEqual(lig.countAtomsHeavyOST(), 40)
        self.assertEqual(lig.countAtomsPDBX(), 40)
        self.assertEqual(lig.countAtomsHeavyPDBX(), 40)
        self.assertEqual(lig.countAtoms(), 74)
        self.assertEqual(lig.countAtomsHeavy(), 40)
        self.assertEqual(lig.countUnknownAtoms(), 0)
        self.assertTrue(lig.isAtomCountConsistent())
        self.assertTrue(lig.checkCoords())
        self.assertEqual(lig.getOccupancies().tolist()[1], 1)
        self.assertEqual(lig.getOccupancies().tolist()[39], 0.5)
        self.assertEqual(lig.getBFactors().tolist()[1], 16.85)
        self.assertEqual(lig.getBFactors().tolist()[39], 10.85)
        self.assertEqual(lig.countO_lt_0_9(), 1)
        self.assertEqual(lig.countNullOccupancies(), 0)
        self.assertEqual(lig.getType(), ['Ligand'])
        self.assertTrue(lig.isType("Ligand"))
        self.assertEqual(lig.getChemClass(), "N")
        self.assertEqual(lig.getChemType(), "M")
        self.assertEqual(lig.getResName(), "6W4")
        self.assertEqual(lig.getEntity(), "5")
        self.assertEqual(lig.getMmCIFChain(), "F")
        self.assertEqual(lig.getAuthChain(), "A")
        self.assertEqual(lig.getMmCIFResNum(), ".")
        self.assertEqual(lig.getOSTResNum(), 1)
        self.assertEqual(lig.getAuthResNum(), "305")
        self.assertEqual(lig.getInsertionCode(), "?")
        self.assertEqual(lig.getAltCode(), "A")
        self.assertAlmostEqual(lig.getRSCC(), 0.959)
        self.assertAlmostEqual(lig.getRSR(), 0.075)
        self.assertTrue(math.isnan(lig.getRSRZ()))
        self.assertEqual(lig.getAuthID(), "A:6W4.305-A")
        self.assertEqual(lig.getMachineAuthID(), "A:6W4:305::A")
        self.assertEqual(lig.getMmCIFID(), "F:6W4..-A")
        self.assertEqual(lig.getMachineMmCIFID(), "F:6W4:.:A")
        self.assertEqual(lig.getOSTID(), "F:6W4.1-A")
        self.assertEqual(lig.getMachineOSTID(), "F:6W4:1:A")
        
    def test_AminoAcidLYS188A(self):
        """ AminoAcid data looks all right"""
        lig = AminoAcid(self.res_5lh4_A_LYS188A['ms'].getAlt("A"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("A"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        self.assertEqual(lig.countAtomsConop(), 25)
        self.assertEqual(lig.countAtomsHeavyConop(), 10)
        self.assertEqual(lig.countAtomsOST(), 9) # Hydrogens not in structure
        self.assertEqual(lig.countAtomsHeavyOST(), 9)
        self.assertEqual(lig.countAtomsPDBX(), 9)
        self.assertEqual(lig.countAtomsHeavyPDBX(), 9)
        self.assertEqual(lig.countAtoms(), 25)
        self.assertEqual(lig.countAtomsHeavy(), 10)
        self.assertEqual(lig.countUnknownAtoms(), 0)
        self.assertTrue(lig.isAtomCountConsistent())
        self.assertTrue(lig.checkCoords())
        self.assertEqual(lig.countO_lt_0_9(), 9)
        self.assertEqual(lig.countNullOccupancies(), 0)
        self.assertEqual(lig.getOccupancies().tolist()[1], 0.5)
        self.assertEqual(lig.getBFactors().tolist()[1], 13.34)
        self.assertEqual(lig.getType(), ['PolymerChainResidue', 'AminoAcid'])
        self.assertTrue(lig.isType("AminoAcid"))
        self.assertEqual(lig.getChemClass(), "L")
        self.assertEqual(lig.getChemType(), "A")
        self.assertEqual(lig.getResName(), "LYS")
        self.assertEqual(lig.getEntity(), "1")
        self.assertEqual(lig.getMmCIFChain(), "A")
        self.assertEqual(lig.getAuthChain(), "A")
        self.assertEqual(lig.getMmCIFResNum(), "170")
        self.assertEqual(lig.getOSTResNum(), 170)
        self.assertEqual(lig.getAuthResNum(), "188")
        self.assertEqual(lig.getInsertionCode(), "A")
        self.assertEqual(lig.getAltCode(), "A")
        self.assertAlmostEqual(lig.getRSCC(), 0.957)
        self.assertAlmostEqual(lig.getRSR(), 0.095)
        self.assertAlmostEqual(lig.getRSRZ(), -0.233)
        self.assertEqual(lig.getMachineMmCIFID(), "A:LYS:170:A")
        self.assertEqual(lig.getMachineMmCIFID(sep='.'), "A.LYS.170.A")
        self.assertEqual(lig.getMmCIFID(), "A:LYS.170-A")
        self.assertEqual(lig.getMachineOSTID(), "A:LYS:170:A")
        self.assertEqual(lig.getMachineOSTID(sep='.'), "A.LYS.170.A")
        self.assertEqual(lig.getOSTID(), "A:LYS.170-A")
        self.assertEqual(lig.getMachineAuthID(), "A:LYS:188:A:A")
        self.assertEqual(lig.getMachineAuthID(sep='.'), "A.LYS.188.A.A")
        self.assertEqual(lig.getAuthID(), "A:LYS.188A-A")
    
    def test_AtomCountConsistency(self):
        """ Can test consistency of atom count of residues"""
        # SER32 is missing atoms
        ser32 = AminoAcid(self.res_5x1g_A_SER32['ms'].getAlt(None),
                self.res_5x1g_A_SER32['pdbx'].getAlt(None),
                self.res_5x1g_A_SER32['ost'],
                self.res_5x1g_A_SER32['conop'])
        self.assertFalse(ser32.isAtomCountConsistent())
        with self.assertRaises(AtomCountError):
            ser32.countAtoms()
        with self.assertRaises(AtomCountError):
            ser32.countAtomsHeavy()
        #LYS188A has the correct number
        lys188A = AminoAcid(self.res_5lh4_A_LYS188A['ms'].getAlt("A"),
                self.res_5lh4_A_LYS188A['pdbx'].getAlt("A"),
                self.res_5lh4_A_LYS188A['ost'],
                self.res_5lh4_A_LYS188A['conop'])
        self.assertTrue(lys188A.isAtomCountConsistent())
        # F:GLU is missing atoms
        glu_f = Ligand(self.res_5n7l_F_GLU['ms'].getAlt(None),
                self.res_5n7l_F_GLU['pdbx'].getAlt(None),
                self.res_5n7l_F_GLU['ost'],
                self.res_5n7l_F_GLU['conop'])
        self.assertFalse(glu_f.isAtomCountConsistent())
        with self.assertRaises(AtomCountError):
            glu_f.countAtoms()
        with self.assertRaises(AtomCountError):
            glu_f.countAtomsHeavy()
        
        # Terminal residue A:GLY80 in 5n7l is missing OXT
        # However we should allow it
        gly80 = AminoAcid(self.res_5n7l_A_GLY80['ms'].getAlt(None),
                self.res_5n7l_A_GLY80['pdbx'].getAlt(None),
                self.res_5n7l_A_GLY80['ost'],
                self.res_5n7l_A_GLY80['conop'])
        self.assertTrue(gly80.isAtomCountConsistent())
        self.assertEqual(gly80.countAtomsHeavy(), 5)
        
        # Terminal residue A:ASN223 in 5lh4 has terminal OXT
        asn223 = AminoAcid(self.res_5lh4_A_ASN223['ms'].getAlt(None),
                self.res_5lh4_A_ASN223['pdbx'].getAlt(None),
                self.res_5lh4_A_ASN223['ost'],
                self.res_5lh4_A_ASN223['conop'])
        self.assertTrue(asn223.isAtomCountConsistent())
        self.assertEqual(asn223.countAtomsHeavy(), 9)
    
    def test_isOutlier(self):
        glu_f = Ligand(self.res_5n7l_F_GLU['ms'].getAlt(None),
                self.res_5n7l_F_GLU['pdbx'].getAlt(None),
                self.res_5n7l_F_GLU['ost'],
                self.res_5n7l_F_GLU['conop'])
        self.assertTrue(glu_f.isOutlier("density"))
        self.assertFalse(glu_f.isOutlier("geometry"))
        self.assertFalse(glu_f.isOutlier("chirality"))
        self.assertFalse(glu_f.isOutlier("clashes"))
        # Reject invalid categories
        with self.assertRaises(ValueError):
            self.assertTrue(glu_f.isOutlier("invalid"))
        
        
        lig2 = Ligand(self.res_6ess_A_4IR400['ms'].getAlt(None),
                self.res_6ess_A_4IR400['pdbx'].getAlt(None),
                self.res_6ess_A_4IR400['ost'],
                self.res_6ess_A_4IR400['conop'])
        self.assertFalse(lig2.isOutlier("density"))
        self.assertFalse(lig2.isOutlier("geometry"))
        self.assertTrue(lig2.isOutlier("chirality"))
        self.assertFalse(lig2.isOutlier("clashes"))
        
        lig3 = Ligand(self.res_5h6c_A_GAI202['ms'].getAlt(None),
                self.res_5h6c_A_GAI202['pdbx'].getAlt(None),
                self.res_5h6c_A_GAI202['ost'],
                self.res_5h6c_A_GAI202['conop'])
        self.assertTrue(lig3.isOutlier("density"))
        self.assertTrue(lig3.isOutlier("geometry"))
        self.assertFalse(lig3.isOutlier("chirality"))
        self.assertTrue(lig3.isOutlier("clashes"))

    def test_NeighborhoodBFactors(self):
        glu_f = Ligand(self.res_5n7l_F_GLU['ms'].getAlt(None),
                self.res_5n7l_F_GLU['pdbx'].getAlt(None),
                self.res_5n7l_F_GLU['ost'],
                self.res_5n7l_F_GLU['conop'])
        target_ratios = [1.1716435811795392, 1.2086688056587473, 1.0530996674630968, 1.1850384890888115, 1.1408420352948663, 1.3838262187295998]
        target_ratios.reverse()
        for atom, b_factors in glu_f.findNeighborhoodBFactors(6, self.doc_5n7l):
            ratio = atom.b_factor / numpy.mean(b_factors)
            self.assertAlmostEqual(ratio, target_ratios.pop())

    def test_NeighborhoodBFactors(self):
        glu_f = Ligand(self.res_5n7l_F_GLU['ms'].getAlt(None),
                self.res_5n7l_F_GLU['pdbx'].getAlt(None),
                self.res_5n7l_F_GLU['ost'],
                self.res_5n7l_F_GLU['conop'])
        nh_atoms = list(glu_f.findNeighborhoodByAtom(6, self.doc_5n7l))
        self.assertEqual(len(nh_atoms), 6)
        self.assertEqual(nh_atoms[3][0].index, 580)
        self.assertEqual(len(nh_atoms[4][1].atoms), 29)

    def test_findClashingAtoms(self):
        clashing_ligand = Ligand(self.res_6ess_A_4IR400['ms'].getAlt(None),
                self.res_6ess_A_4IR400['pdbx'].getAlt(None),
                self.res_6ess_A_4IR400['ost'],
                self.res_6ess_A_4IR400['conop'])
        clashes = list(clashing_ligand.findClashingAtoms())
        self.assertEqual(len(clashes), 1)
        self.assertFalse(clashes[0][0].IsValid())

        clashing_ligand = Ligand(self.res_5x1g_C_ARG535['ms'].getAlt(None),
                self.res_5x1g_C_ARG535['pdbx'].getAlt(None),
                self.res_5x1g_C_ARG535['ost'],
                self.res_5x1g_C_ARG535['conop'])
        clashes = list(clashing_ligand.findClashingAtoms())
        self.assertEqual(len(clashes), 2)
        self.assertTrue(clashes[0][0].IsValid())
        self.assertTrue(clashes[1][0].IsValid())
        self.assertEqual({x[1] for x in clashes}, {'N', 'O'})

    def test_hasClashingPartialOccupancyAtoms(self):
        clashing_ligand = Ligand(self.res_6d0g_A_BR402['ms'].getAlt(None),
                self.res_6d0g_A_BR402['pdbx'].getAlt(None),
                self.res_6d0g_A_BR402['ost'],
                self.res_6d0g_A_BR402['conop'])
        self.assertTrue(clashing_ligand.hasClashingPartialOccupancyAtoms())
        nonclashing_ligand = Ligand(self.res_5x1g_C_ARG535['ms'].getAlt(None),
                self.res_5x1g_C_ARG535['pdbx'].getAlt(None),
                self.res_5x1g_C_ARG535['ost'],
                self.res_5x1g_C_ARG535['conop'])
        self.assertFalse(nonclashing_ligand.hasClashingPartialOccupancyAtoms())
        # missing H do not count
        invalid_clashing_ligand = Ligand(self.res_6ess_A_4IR400['ms'].getAlt(None),
                self.res_6ess_A_4IR400['pdbx'].getAlt(None),
                self.res_6ess_A_4IR400['ost'],
                self.res_6ess_A_4IR400['conop'])
        self.assertFalse(invalid_clashing_ligand.hasClashingPartialOccupancyAtoms())


class TestResidueAlternativeConfigurations(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5lh4_validation_fn = os.path.join(cls.data_dir, "5lh4_validation.xml.gz")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")
        # Get some 5lh4 residues
        doc_5lh4 = ValidationFactory(cls.pdbx_5lh4_validation_fn, cls.pdbx_5lh4_cif_fn).getValidation()
        xml_5lh4 = doc_5lh4.getValidationXML()
        cls.comp_lib = doc_5lh4.getCompoundLib()
        cls.ost_ent = doc_5lh4.getEntity()  # Entity needs to stay in memory
        cls.res_5lh4_A_GLY188 = {
            'ms': xml_5lh4.getModelledSubgroups(1, "A", 188, 1, " "),
            'pdbx': doc_5lh4.getPdbxResidueAuthorPosition("A", "188", "?"),
            'ost': doc_5lh4.getEntity().FindChain("A").FindResidue(169),
            'conop':  cls.comp_lib.FindCompound("GLY")
        }
        cls.res_5lh4_A_LYS188A = {
            'ms': xml_5lh4.getModelledSubgroups(1, "A", 188, 1, "A"),
            'pdbx': doc_5lh4.getPdbxResidueAuthorPosition("A", "188", "A"),
            'ost': doc_5lh4.getEntity().FindChain("A").FindResidue(170),
            'conop':  cls.comp_lib.FindCompound("LYS")
        }
        cls.res_5lh4_A_6W4 = {
            'ms': xml_5lh4.getModelledSubgroups(5, "A", 305, 1, " "),
            'pdbx': doc_5lh4.getPdbxResidueMmCIFPosition("F", ".", "5"),
            'ost': doc_5lh4.getEntity().FindChain("F").FindResidue(1),
            'conop':  cls.comp_lib.FindCompound("6W4")
        }
        
    def test__init(self):
        """ Can instantiate ResidueAlternativeConfigurations"""
        ResidueAlternativeConfigurations(self.res_5lh4_A_GLY188['ms'],
                                         self.res_5lh4_A_GLY188['pdbx'],
                                         self.res_5lh4_A_GLY188['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        ResidueAlternativeConfigurations(self.res_5lh4_A_LYS188A['ms'],
                                         self.res_5lh4_A_LYS188A['pdbx'],
                                         self.res_5lh4_A_LYS188A['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        ResidueAlternativeConfigurations(self.res_5lh4_A_6W4['ms'],
                                         self.res_5lh4_A_6W4['pdbx'],
                                         self.res_5lh4_A_6W4['ost'],
                                         self.comp_lib,
                                         Ligand)
    
    def test_AltLigand(self):
        """ Can get alternative configurations as Ligand"""
        lig = ResidueAlternativeConfigurations(self.res_5lh4_A_6W4['ms'],
                                         self.res_5lh4_A_6W4['pdbx'],
                                         self.res_5lh4_A_6W4['ost'],
                                         self.comp_lib,
                                         Ligand)
        self.assertTrue(lig.hasAlt())
        self.assertEqual(lig.listAlt(), {"A", "B"})
        self.assertIsInstance(lig.getAlt("A"), Ligand)
        self.assertIsInstance(lig.getAlt("B"), Ligand)
        with self.assertRaises(KeyError):
            lig.getAlt(None)
        
    def test_AltAminoAcid(self):
        """ Can get alternative configurations as AminoAcid"""
        aa = ResidueAlternativeConfigurations(self.res_5lh4_A_LYS188A['ms'],
                                         self.res_5lh4_A_LYS188A['pdbx'],
                                         self.res_5lh4_A_LYS188A['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        self.assertTrue(aa.hasAlt())
        self.assertEqual(aa.listAlt(), {"A", "B"})
        self.assertIsInstance(aa.getAlt("A"), AminoAcid)
        self.assertIsInstance(aa.getAlt("B"), AminoAcid)
        with self.assertRaises(KeyError):
            aa.getAlt(None)
        
    def test_AltNoAlt(self):
        """ Can get single alternative configuration"""
        aa = ResidueAlternativeConfigurations(self.res_5lh4_A_GLY188['ms'],
                                         self.res_5lh4_A_GLY188['pdbx'],
                                         self.res_5lh4_A_GLY188['ost'],
                                              self.comp_lib,
                                         AminoAcid)
        self.assertFalse(aa.hasAlt())
        self.assertEqual(aa.listAlt(), {"."})
        self.assertIsInstance(aa.getAlt(None), AminoAcid)
        self.assertIsInstance(aa.getAlt(""), AminoAcid)
        self.assertIsInstance(aa.getAlt(" "), AminoAcid)
        self.assertIsInstance(aa.getAlt("."), AminoAcid)
        with self.assertRaises(KeyError):
            aa.getAlt("A")
        with self.assertRaises(KeyError):
            aa.getAlt("B")
    
    def test_equality(self):
        """ Can test equality of residues"""
        ligA = Ligand(self.res_5lh4_A_6W4['ms'].getAlt("A"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("A"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        ligB = Ligand(self.res_5lh4_A_6W4['ms'].getAlt("B"),
                self.res_5lh4_A_6W4['pdbx'].getAlt("B"),
                self.res_5lh4_A_6W4['ost'],
                self.res_5lh4_A_6W4['conop'])
        ligAll = ResidueAlternativeConfigurations(self.res_5lh4_A_6W4['ms'],
                                         self.res_5lh4_A_6W4['pdbx'],
                                         self.res_5lh4_A_6W4['ost'],
                                         self.comp_lib,
                                         Ligand)
        self.assertEqual(ligAll.getAlt("A"), ligA)
        self.assertEqual(ligAll.getAlt("B"), ligB)
        self.assertTrue(ligAll.getAlt("A") == ligA)
        self.assertTrue(ligAll.getAlt("B") == ligB)
        self.assertFalse(ligAll.getAlt("A") == ligB)
        self.assertFalse(ligAll.getAlt("B") == ligA)
    
    def test_ID(self):
        aa = ResidueAlternativeConfigurations(self.res_5lh4_A_GLY188['ms'],
                                         self.res_5lh4_A_GLY188['pdbx'],
                                         self.res_5lh4_A_GLY188['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        self.assertEqual(aa.getAuthID(), "A:GLY.188")
        self.assertEqual(aa.getMachineAuthID(), "A:GLY:188:")
        self.assertEqual(aa.getMachineAuthID("."), "A.GLY.188.")
        self.assertEqual(aa.getMmCIFID(), "A:GLY.169")
        self.assertEqual(aa.getMachineMmCIFID(), "A:GLY:169")
        self.assertEqual(aa.getMachineMmCIFID("."), "A.GLY.169")
        self.assertEqual(aa.getOSTID(), "A:GLY.169")
        self.assertEqual(aa.getMachineOSTID(), "A:GLY:169")
        self.assertEqual(aa.getMachineOSTID("."), "A.GLY.169")
        
        aa = ResidueAlternativeConfigurations(self.res_5lh4_A_LYS188A['ms'],
                                         self.res_5lh4_A_LYS188A['pdbx'],
                                         self.res_5lh4_A_LYS188A['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        self.assertEqual(aa.getAuthID(), "A:LYS.188A")
        self.assertEqual(aa.getMachineAuthID(), "A:LYS:188:A")
        self.assertEqual(aa.getMachineAuthID("."), "A.LYS.188.A")
        self.assertEqual(aa.getMmCIFID(), "A:LYS.170")
        self.assertEqual(aa.getMachineMmCIFID(), "A:LYS:170")
        self.assertEqual(aa.getMachineMmCIFID("."), "A.LYS.170")
        self.assertEqual(aa.getOSTID(), "A:LYS.170")
        self.assertEqual(aa.getMachineOSTID(), "A:LYS:170")
        self.assertEqual(aa.getMachineOSTID("."), "A.LYS.170")
    
    def test_iter(self):
        aa = ResidueAlternativeConfigurations(self.res_5lh4_A_LYS188A['ms'],
                                         self.res_5lh4_A_LYS188A['pdbx'],
                                         self.res_5lh4_A_LYS188A['ost'],
                                         self.comp_lib,
                                         AminoAcid)
        alts = list(aa.iter())
        self.assertEqual(len(alts), 2)

        
if __name__ == '__main__':
    unittest.main()
