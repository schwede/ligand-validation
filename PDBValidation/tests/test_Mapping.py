import os
import unittest

from ost import io, GetSharedDataPath, conop

from ..PDBXReader import PDBX
from ..Mapping import *

class TestMapping(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5tgu_cif_fn = os.path.join(cls.data_dir, "5tgu.cif.gz")
        cls.pdbx_5tgu_atom_sites = PDBX.CreateFromGZIP(cls.pdbx_5tgu_cif_fn).getAtomSites()

    def test_init(self):
        "PDBMapper can be instanciated"
        PDBMapper(self.pdbx_5tgu_atom_sites)

    def test_mmcifToAuthorChain(self):
        "5tgu: A -> A "
        mapper = PDBMapper(self.pdbx_5tgu_atom_sites)
        mapped_chain, extra = mapper.mmcifToAuthor("A")
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')

    def test_mmcifToAuthorChainResNum(self):
        "5tgu: A123 -> A129; A124 -> A129A "
        mapper = PDBMapper(self.pdbx_5tgu_atom_sites)
        mapped_chain, mapped_resnum, icode, extra = mapper.mmcifToAuthor("A", 123)
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(mapped_resnum, '129')
        self.assertEqual(icode, None)
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
        mapped_chain, mapped_resnum, icode, extra = mapper.mmcifToAuthor("A", 124)
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(mapped_resnum, '129')
        self.assertEqual(icode, "A")
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
    
    def test_authorToMmCIFChain(self):
        "5tgu: A -> A "
        mapper = PDBMapper(self.pdbx_5tgu_atom_sites)
        mapped_chain, extra = mapper.authorToMmCIF("A")
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
        self.assertEqual(extra['label_entity_id'].iloc[0], '1')

    def test_authorToMmCIFResNum(self):
        "5tgu: A129 -> A123; A129A -> A124"
        mapper = PDBMapper(self.pdbx_5tgu_atom_sites)
        mapped_chain, mapped_resnum, extra = mapper.authorToMmCIF("A", '129')
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(mapped_resnum, '123')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
        self.assertEqual(extra['label_entity_id'].iloc[0], '1')
        mapped_chain, mapped_resnum, extra = mapper.authorToMmCIF("A", '129', "A")
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(mapped_resnum, '124')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
        self.assertEqual(extra['label_entity_id'].iloc[0], '1')

    def test_authorToMmCIFResNumIcodes(self):
        "5tgu: A129 -> A123 with various icodes"
        mapper = PDBMapper(self.pdbx_5tgu_atom_sites)
        for icode in [None, "", " ", "?"]:
            mapped_chain, mapped_resnum, extra = mapper.authorToMmCIF("A", '129', icode)
            self.assertEqual(mapped_chain, 'A')
            self.assertEqual(mapped_resnum, '123')
            self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')
            self.assertEqual(extra['label_entity_id'].iloc[0], '1')

class TestBioUnitMapping(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5za2_cif_fn = os.path.join(cls.data_dir, "5za2.cif.gz")
        cls.pdbx_5za2_atom_sites = PDBX.CreateFromGZIP(cls.pdbx_5za2_cif_fn).getAtomSites()

        ost_ent, seqres, mmcif_info = io.LoadMMCIF(cls.pdbx_5za2_cif_fn, seqres=True, info=True)
        cls.biounits_5za2_1 = mmcif_info.biounits[0].PDBize(ost_ent, seqres)
        cls.biounits_5za2_2 = mmcif_info.biounits[1].PDBize(ost_ent, seqres)

    def test_init(self):
        "PDBMapper can be instanciated"
        BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_1)
        BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_2)

    def test_buToMmCIFChain(self):
        " Can convert pdbized biounit to mmCIF chain position"
        bu_mapper_1 = BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_1)
        bu_mapper_2 = BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_2)

        mapped_chain, extra = bu_mapper_1.buToMmCIF("A")
        self.assertEqual(mapped_chain, 'A')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')

        mapped_chain, extra = bu_mapper_2.buToMmCIF("A")
        self.assertEqual(mapped_chain, 'B')
        self.assertEqual(extra['pdbx_PDB_model_num'].iloc[0], '1')

    def test_buToMmCIF_ligand_waters(self):
        " Cannot convert ligand/water chains"
        bu_mapper_1 = BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_1)
        with self.assertRaises(ValueError):
            bu_mapper_1.buToMmCIF("_")
        with self.assertRaises(ValueError):
            bu_mapper_1.buToMmCIF("-")

    def test_buToMmCIFResNum(self):
        " Can convert pdbized ligand to mmCIF position"
        bu_mapper_1 = BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_1)
        bu_mapper_2 = BioUnitMapper(self.pdbx_5za2_atom_sites, self.biounits_5za2_2)

        mapped_chain, mapped_resnum, extra = bu_mapper_1.buToMmCIF("_", 1)
        self.assertEqual(mapped_chain, 'C')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'NXL')

        mapped_chain, mapped_resnum, extra = bu_mapper_1.buToMmCIF("_", 2)
        self.assertEqual(mapped_chain, 'D')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'GOL')

        mapped_chain, mapped_resnum, extra = bu_mapper_1.buToMmCIF("_", 3)
        self.assertEqual(mapped_chain, 'E')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'GOL')

        mapped_chain, mapped_resnum, extra = bu_mapper_1.buToMmCIF("_", 4)
        self.assertEqual(mapped_chain, 'F')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'GOL')

        mapped_chain, mapped_resnum, extra = bu_mapper_1.buToMmCIF("_", 5)
        self.assertEqual(mapped_chain, 'G')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'EPE')

        # Note: _:NXL.1 is present in alternative conformation B only.
        # A future version of OST may read no ligand at all in biounit 2 which could make this test fail
        # In that case, either alternative configuration B should be selected (if possible) or the test should be
        # removed
        mapped_chain, mapped_resnum, extra = bu_mapper_2.buToMmCIF("_", 1)
        self.assertEqual(mapped_chain, 'H')
        self.assertEqual(mapped_resnum, '.')
        self.assertEqual(extra['label_comp_id'].iloc[0], 'NXL')

if __name__ == '__main__':
    unittest.main()
