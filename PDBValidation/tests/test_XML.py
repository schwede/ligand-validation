import os
import unittest

from ost import io, conop, GetSharedDataPath

from ..XML import *

class TestXML(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5ou3_validation_fn = os.path.join(cls.data_dir, "5ou3_validation.xml.gz")
        cls.pdbx_5lh4_validation_fn = os.path.join(cls.data_dir, "5lh4_validation.xml.gz")
        cls.pdbx_5za2_validation_fn = os.path.join(cls.data_dir, "5za2_validation.xml.gz")

    def test_init(self):
        "PDBValidationXML can be instanciated"
        PDBValidationXML(self.pdbx_5ou3_validation_fn)
        PDBValidationXML(self.pdbx_5lh4_validation_fn)
        PDBValidationXML(self.pdbx_5za2_validation_fn)
        
    def test_Entry(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        entry = xml.getEntry()
        self.assertEqual(entry.get("PDB-resolution"), '1.60')
        self.assertEqual(entry.get("pdbid"), '5OU3')
        
    def test_ElementToDict(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        entry = xml.getEntry()
        entry_dict = elementToDict(entry)
        self.assertEqual(entry_dict["PDB-resolution"], '1.60')
        self.assertEqual(entry_dict["pdbid"], '5OU3')
    
    def test_ModelledSubgroupNotFound(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        with self.assertRaises(ModelledSubgroupNotFound):
            xml.getModelledSubgroup(1, "A", "LEU", 315, 1)

    def test_ModelledSubgroupFound5OU3(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        ms = xml.getModelledSubgroup(1, "A", "VAL", 316, 1)
        self.assertIsInstance(ms, ModelledSubgroup)
        self.assertEqual(ms.getAuthChain(), "A")
        self.assertEqual(ms.get("chain"), "A")
        self.assertEqual(ms.getResName(), "VAL")
        self.assertEqual(ms.get("resname"), "VAL")
        self.assertEqual(ms.getAuthResNum(), "316")
        self.assertEqual(ms.get("resnum"), "316")
        self.assertEqual(ms.getEntity(), "1")
        self.assertEqual(ms.get("ent"), "1")
        self.assertIsNone(ms.getAltCode())
        self.assertEqual(ms.get("altcode"), " ")
        self.assertIsNone(ms.getInsertionCode())
        self.assertEqual(ms.get("icode"), " ")
        self.assertEqual(ms.getModel(), "1")
        self.assertEqual(ms.get("model"), "1")
        self.assertEqual(ms.get("phi"), "-135.4")

    def test_ModelledSubgroupFound5LH4(self):
        """ ModelledSubgroup with altcode and icode"""
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        ms = xml.getModelledSubgroup(1, "A", "LYS", 188, 1, "A", "A")
        self.assertIsInstance(ms, ModelledSubgroup)
        self.assertEqual(ms.getAuthChain(), "A")
        self.assertEqual(ms.get("chain"), "A")
        self.assertEqual(ms.getResName(), "LYS")
        self.assertEqual(ms.get("resname"), "LYS")
        self.assertEqual(ms.getAuthResNum(), "188")
        self.assertEqual(ms.get("resnum"), "188")
        self.assertEqual(ms.getEntity(), "1")
        self.assertEqual(ms.get("ent"), "1")
        self.assertEqual(ms.getAltCode(), "A")
        self.assertEqual(ms.get("altcode"), "A")
        self.assertEqual(ms.getInsertionCode(), "A")
        self.assertEqual(ms.get("icode"), "A")
        self.assertEqual(ms.getModel(), "1")
        self.assertEqual(ms.get("model"), "1")
        self.assertEqual(ms.get("phi"), "-136.8")
    
    def test_getModelledSubgroupIcodeNone(self):
        """ Test different ways to pass empty icode"""
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        ms1 = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, " ", " ") # Fully defined
        ms1_dict = elementToDict(ms1)
        ms2 = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, None, " ") # icode = None
        self.assertEqual(ms1_dict, elementToDict(ms2))
        ms3 = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, " ", None) # altcode = None
        self.assertEqual(ms1_dict, elementToDict(ms3))
        ms4 = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, None, None) # combination
        self.assertEqual(ms1_dict, elementToDict(ms4))
        ms5 = xml.getModelledSubgroup(1, "A", None, 188, 1, " ", " ") # resname = None
        self.assertEqual(ms1_dict, elementToDict(ms5))
        ms6 = xml.getModelledSubgroup(1, "A", None, 188, 1, None, " ") # combination
        self.assertEqual(ms1_dict, elementToDict(ms6))
        ms7 = xml.getModelledSubgroup(1, "A", None, 188, 1, " ", None) # combination
        self.assertEqual(ms1_dict, elementToDict(ms7))
        ms8 = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, None, None) # combination
        self.assertEqual(ms1_dict, elementToDict(ms8))
        
    def test_ModelledSubgroupAmbiguous(self):
        """ ModelledSubgroup with altcode and icode is not found if icode or altcode isn't provided"""
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        # Default altcode is " " so this won't be found:
        with self.assertRaises(ModelledSubgroupNotFound):
            ms = xml.getModelledSubgroup(1, "A", "LYS", 188, 1)
        with self.assertRaises(ModelledSubgroupNotFound):
            ms = xml.getModelledSubgroup(1, "A", "LYS", 188, 1, altcode = "A")
        with self.assertRaises(ModelledSubgroupNotFound):
            ms = xml.getModelledSubgroup(1, "A", "LYS", 188, 1, icode = "A")
    
    def test_findModelledSubgroups(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        ms_gen = xml.findModelledSubgroups()
        ms_list = list(ms_gen)
        self.assertEqual(len(ms_list), 348)
        self.assertIsInstance(ms_list[0], ModelledSubgroup)

    def test_ModelledSubgroups(self):
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        subgroups = xml.getModelledSubgroups(1, "A", 188, 1, "A")
        self.assertIsInstance(subgroups, ModelledSubgroups)
        self.assertTrue(subgroups.hasAlt())
        self.assertEqual(subgroups.listAlt(), {'A', 'B'})
        # Test Additional content
        self.assertEqual(subgroups.getAuthChain(), "A")
        self.assertEqual(subgroups.getResName(), "LYS")
        self.assertEqual(subgroups.getAuthResNum(), "188")
        self.assertEqual(subgroups.getEntity(), "1")
        self.assertEqual(subgroups.getInsertionCode(), "A")

    def test_ModelledSubgroupsGetAlt(self):
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        subgroups = xml.getModelledSubgroups(1, "A", 188, 1, "A")
        # Test that getAlt gets the same element as a direct getModelledSubgroup call
        sub_alt_A = subgroups.getAlt("A")
        self.assertIsInstance(sub_alt_A, ModelledSubgroup)
        sub_alt_a_direct = xml.getModelledSubgroup(1, "A", "LYS", 188, 1, "A", "A")
        # Comparing instances directly would result in a failure
        # sub_alt_A != sub_alt_a_direct
        # Because they are different instances.
        # However comparison of dictionaries work
        sub_alt_A_dict = elementToDict(sub_alt_A)
        sub_alt_a_direct_dict = elementToDict(sub_alt_a_direct)
        self.assertEqual(sub_alt_A_dict, sub_alt_a_direct_dict)
        # Ensure we cannot getAlt(None)
        with self.assertRaises(KeyError):
            subgroups.getAlt(None)
        with self.assertRaises(KeyError):
            subgroups.getAlt(".")
        with self.assertRaises(KeyError):
            subgroups.getAlt("")
        with self.assertRaises(KeyError):
            subgroups.getAlt(" ")
        
    def test_ModelledSubgroupsIter(self):
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        subgroups = xml.getModelledSubgroups(1, "A", 188, 1, "A")
        alts = list(subgroups.iter())
        self.assertEqual(len(alts), 2)
    
    def test_ModelledSubgroupsGetAltNone(self):
        xml = PDBValidationXML(self.pdbx_5lh4_validation_fn)
        sub_alt_a_direct = xml.getModelledSubgroup(1, "A", "GLY", 188, 1, None, None)
        subgroups = xml.getModelledSubgroups(1, "A", 188, 1, "")
        self.assertFalse(subgroups.hasAlt())
        self.assertEqual(subgroups.listAlt(), {" "})
        # getAlt with various inputs:
        self.assertEqual(elementToDict(subgroups.getAlt(None)), elementToDict(sub_alt_a_direct))
        self.assertEqual(elementToDict(subgroups.getAlt(".")), elementToDict(sub_alt_a_direct))
        self.assertEqual(elementToDict(subgroups.getAlt("")), elementToDict(sub_alt_a_direct))
        self.assertEqual(elementToDict(subgroups.getAlt(" ")), elementToDict(sub_alt_a_direct))
        with self.assertRaises(KeyError):
            subgroups.getAlt("A")
        
    def test_NoneResNames(self):
        # 5ZA2 Has B:SEP.64-A and B:SER.64-B
        xml = PDBValidationXML(self.pdbx_5za2_validation_fn)
        
        # With explicit resname
        sepA = xml.getModelledSubgroup(entity=2, chain="B", resname="SEP", 
                                       resnum=64, model=1, icode=None, altcode="A")
        self.assertEqual(sepA.getResName(), "SEP")
        # With None resname
        sepA = xml.getModelledSubgroup(entity=2, chain="B", resname=None, 
                                       resnum=64, model=1, icode=None, altcode="A")
        self.assertEqual(sepA.getResName(), "SEP")
        
        # Wrong resname crashes
        with self.assertRaises(ModelledSubgroupNotFound):
            xml.getModelledSubgroup(entity=2, chain="B", resname="SER",
                                       resnum=64, model=1, icode=None, altcode="A")

        # Test SER-B
        serB = xml.getModelledSubgroup(entity=2, chain="B", resname="SER",
                                       resnum=64, model=1, icode=None, altcode="B")
        self.assertEqual(serB.getResName(), "SER")
        # With None resname
        serB = xml.getModelledSubgroup(entity=2, chain="B", resname=None,
                                       resnum=64, model=1, icode=None, altcode="B")
        self.assertEqual(serB.getResName(), "SER")
        # Wrong resname crashes
        with self.assertRaises(ModelledSubgroupNotFound):
            xml.getModelledSubgroup(entity=2, chain="B", resname="SEP",
                                       resnum=64, model=1, icode=None, altcode="B")
        
    def test_SubgroupsCanHaveDifferentResNames(self):
        # 5ZA2 Has B:SEP.64-A and B:SER.64-B
        xml = PDBValidationXML(self.pdbx_5za2_validation_fn)
        subgroups = xml.getModelledSubgroups(entity=2, chain="B",
                                       resnum=64, model=1, icode=None)
        
        self.assertTrue(subgroups.hasAlt())
        self.assertEqual(subgroups.listAlt(), {"A", "B"})
        self.assertEqual(subgroups.getResName(), "SEP|SER")
        self.assertEqual(subgroups.getAlt("A").getResName(), "SEP")
        self.assertEqual(subgroups.getAlt("B").getResName(), "SER")

    def test_findClashes(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        subgroup = xml.getModelledSubgroup(entity=1, chain="A", resname="LYS",
                                       resnum=57, model=1, icode=None, altcode = None)
        clash_generator = subgroup.findClashes()
        clash_list = list(clash_generator)
        self.assertEqual(len(clash_list), 2)
        self.assertEqual(clash_list[0].tag, "clash")
        self.assertEqual(clash_list[1].tag, "clash")

        subgroup = xml.getModelledSubgroup(entity=4, chain="A", resname="HOH",
                                       resnum=604, model=1, icode=None, altcode = None)
        clash_generator = subgroup.findClashes()
        clash_list = list(clash_generator)
        self.assertEqual(len(clash_list), 1)
        self.assertEqual(clash_list[0].tag, "symm-clash")

    def test_getClashAtomNames(self):
        xml = PDBValidationXML(self.pdbx_5ou3_validation_fn)
        subgroup = xml.getModelledSubgroup(entity=1, chain="A", resname="LYS",
                                       resnum=57, model=1, icode=None, altcode = None)
        clash_names = subgroup.getClashAtomNames()
        self.assertEqual(clash_names, set(['NZ', 'HE3']))

        subgroup = xml.getModelledSubgroup(entity=4, chain="A", resname="HOH",
                                       resnum=604, model=1, icode=None, altcode = None)
        clash_names = subgroup.getClashAtomNames()
        self.assertEqual(clash_names, set(['O']))

        
if __name__ == '__main__':
    unittest.main()
