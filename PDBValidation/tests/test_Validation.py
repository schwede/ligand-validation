import os
import unittest

import pandas
import ost
from ost import io, conop, GetSharedDataPath

from ..Mapping import *
from ..PDBXReader import *
from ..Residue import *
from ..Validation import *
from ..ValidationFactory import *
from ..XML import *

class TestPDBValidationInit(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
    
    @classmethod
    def setUpClass(cls):
        data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        pdbx_5lh4_validation_fn = os.path.join(data_dir, "5lh4_validation.xml.gz")
        pdbx_5lh4_cif_fn = os.path.join(data_dir, "5lh4.cif.gz")
        cls.pdbx_5lh4_factory = ValidationFactory(pdbx_5lh4_validation_fn, pdbx_5lh4_cif_fn)
        pdbx_5ou3_validation_fn = os.path.join(data_dir, "5ou3_validation.xml.gz")
        pdbx_5ou3_cif_fn = os.path.join(data_dir, "5ou3.cif.gz")
        cls.pdbx_5ou3_factory = ValidationFactory(pdbx_5ou3_validation_fn, pdbx_5ou3_cif_fn)
        pdbx_5x1g_validation_fn = os.path.join(data_dir, "5x1g_validation.xml.gz")
        pdbx_5x1g_cif_fn = os.path.join(data_dir, "5x1g.cif.gz")
        cls.pdbx_5x1g_factory = ValidationFactory(pdbx_5x1g_validation_fn, pdbx_5x1g_cif_fn)
        
    def test_getPDBId(self):
        doc5ou3 = self.pdbx_5ou3_factory.getValidation()
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        
        self.assertEqual(doc5ou3.getPDBId(), "5OU3")
        self.assertEqual(doc5lh4.getPDBId(), "5LH4")
        
    def test_getCompoundLib(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getCompoundLib(), ost.conop.CompoundLib)
        
    def test_getPdbx(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getPdbx(), PDBX)
        
    def test_getReflnsShell(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getReflnsShell(), pandas.DataFrame)
        self.assertEqual(len(doc5lh4.getReflnsShell()['d_res_high'].iloc[0]), 1)
    
    def test_getEntity(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getEntity(), ost.mol.EntityHandle)
    
    def test_getValidationXML(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getValidationXML(), PDBValidationXML)
    
    def test_getAtomSites(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getAtomSites(), PDBXAtomSites)

    def test_getMapper(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(doc5lh4.getMapper(), PDBMapper)
    
    def test_getMethod(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertEqual(doc5lh4.getMethod(), 'X-RAY DIFFRACTION')

    def test_getResolution(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertAlmostEqual(doc5lh4.getResolution(), 1.37)
        
    
    def test_getReflectionsResolution(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertAlmostEqual(doc5lh4.getReflectionsResolution(), 1.37)
    
    def test_getMeanIOverSigIObs(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertIsNone(doc5lh4.getMeanIOverSigIObs())
        
    def test_hasNonPolymerLigands(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertTrue(doc5lh4.hasNonPolymerLigands())

    def test_calcMolProbityOverallScore(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertAlmostEqual(doc5lh4.calcMolProbityOverallScore(), 1.1378874620744535)

    def test_calcMeanStructureBFactor(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        doc5ou3 = self.pdbx_5ou3_factory.getValidation()
        doc5x1g = self.pdbx_5x1g_factory.getValidation()
        self.assertEqual(doc5lh4.calcMeanStructureBFactor(), 17.158937867144836)
        self.assertEqual(doc5ou3.calcMeanStructureBFactor(), 30.028910273494958)
        self.assertEqual(doc5x1g.calcMeanStructureBFactor(), 142.64903210055442)

    def test_calcMedianStructureBFactor(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        doc5ou3 = self.pdbx_5ou3_factory.getValidation()
        doc5x1g = self.pdbx_5x1g_factory.getValidation()
        self.assertEqual(doc5lh4.calcMedianStructureBFactor(), 13.620000839233398)
        self.assertEqual(doc5ou3.calcMedianStructureBFactor(), 27.510000228881836)
        self.assertEqual(doc5x1g.calcMedianStructureBFactor(), 135.75)
        
    def test_isModelledSubgroupNonPolymerLigand(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        lys188 = doc5lh4.getValidationXML().getModelledSubgroup(1, "A", "LYS", 188, 1, "A", "A")
        self.assertFalse(doc5lh4.isModelledSubgroupNonPolymerLigand(lys188))
        lys188 = doc5lh4.getValidationXML().getModelledSubgroup(5, "A", "6W4", 305, 1, None, "A")
        self.assertTrue(doc5lh4.isModelledSubgroupNonPolymerLigand(lys188))
    
    def test_findModelledSubgroupNonPolymerLigands(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        modelled_subgroup_list = list(doc5lh4.findModelledSubgroupNonPolymerLigands())
        self.assertEqual(len(modelled_subgroup_list), 6)
        for ms in modelled_subgroup_list:
            self.assertIsInstance(ms, ModelledSubgroup)
        # Check basic content
        resnames = [ms.getResName() for ms in modelled_subgroup_list]
        self.assertIn("6W4", resnames)
        self.assertIn("SO4", resnames)
        self.assertIn("DMS", resnames)
        self.assertIn("CA", resnames)
        
    def test_getPdbxResidueAuthorPosition(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        a301 = doc5lh4.getPdbxResidueAuthorPosition("A", "301", "?")
        self.assertIsInstance(a301, PDBXResidueAtomSites)
        self.assertEqual(a301.getResName(), "DMS")
        
    def test_getPdbxResidueMmCIFPosition(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        f5 = doc5lh4.getPdbxResidueMmCIFPosition("F", ".", "5")
        self.assertIsInstance(f5, PDBXResidueAtomSites)
        self.assertEqual(f5.getResName(), "6W4")
        
    def test_getNonPolymerLigands(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        nonpoly = doc5lh4.getNonPolymerLigands()
        self.assertIsInstance(nonpoly, pandas.DataFrame)
        self.assertTrue(pandas.Series.all(nonpoly.entity_id == [x for x in "23456"]))
        self.assertTrue(pandas.Series.all(nonpoly.comp_id == ["DMS", "SO4", "CA", "6W4", "HOH"]))
        self.assertTrue(nonpoly.name.iloc[0], 'DIMETHYL SULFOXIDE')
        self.assertTrue(nonpoly.name.iloc[1], 'SULFATE ION')
        self.assertTrue(nonpoly.name.iloc[2], 'CALCIUM ION')
        self.assertTrue(nonpoly.name.iloc[3], '(2~{S},4~{S})-1-[4-(aminomethyl)phenyl]carbonyl-4-(4-cyclopropyl-1,2,3-triazol-1-yl)-~{N}-(2,2-diphenylethyl)pyrrolidine-2-carboxamide')
        self.assertTrue(nonpoly.name.iloc[4], 'water')
    
    def test_countAtoms(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        self.assertEqual(doc5lh4.countAtoms(), 2047)
    
    def test_getOSTLigand(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        lig = doc5lh4.getOSTLigand("F", "6W4", 1, "")
        self.assertIsInstance(lig, ost.mol.ResidueHandle)
        self.assertEqual(lig.name, "6W4")
    
    def test_findNonPolymerLigands(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        ligand_list = list(doc5lh4.findNonPolymerLigands())
        self.assertEqual(len(ligand_list), 5)
        #for ligand in ligand_list:
        #    self.assertIsInstance(ligand[0], Ligand)
        self.assertFalse(ligand_list[0].hasAlt())
        self.assertFalse(ligand_list[1].hasAlt())
        self.assertFalse(ligand_list[2].hasAlt())
        self.assertFalse(ligand_list[3].hasAlt())
        self.assertTrue(ligand_list[4].hasAlt())
        self.assertEqual(ligand_list[4].listAlt(), {'A', "B"})
        self.assertEqual(ligand_list[4].getAlt("A").getResName(), "6W4")
        self.assertEqual(ligand_list[4].getAlt("B").getResName(), "6W4")
        
    def test_NoLigand(self):
        doc5x1g = self.pdbx_5x1g_factory.getValidation()
        self.assertEqual(len(list(doc5x1g.findNonPolymerLigands())), 0)
    
    def test_failedReproduceRValues(self):
        doc5lh4 = self.pdbx_5lh4_factory.getValidation()
        doc5ou3 = self.pdbx_5ou3_factory.getValidation()
        doc5x1g = self.pdbx_5x1g_factory.getValidation()
        self.assertTrue(doc5ou3.failedReproduceRValues())
        self.assertFalse(doc5lh4.failedReproduceRValues())
        self.assertTrue(doc5x1g.failedReproduceRValues())
    
if __name__ == '__main__':
    unittest.main()
