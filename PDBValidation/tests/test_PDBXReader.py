import gzip
import os
import unittest

import pandas

from ost import io, conop, GetSharedDataPath

from ..PDBXReader import *

class TestPDBXReader(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5ou3_cif_fn = os.path.join(cls.data_dir, "5ou3.cif.gz")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")

    def test_init(self):
        "PDBXReader can be instanciated"
        with gzip.open(self.pdbx_5ou3_cif_fn, 'rt', encoding='utf-8') as f:
            pdbx = PDBX(f)
            self.assertIsInstance(pdbx, PDBX)
    
    def test_CreateFromGZIP(self):
        "PDBXReader can be created from GZIP"
        pdbx = PDBX.CreateFromGZIP(self.pdbx_5ou3_cif_fn)
        self.assertIsInstance(pdbx, PDBX)
        pdbx = PDBX.CreateFromGZIP(self.pdbx_5lh4_cif_fn)
        self.assertIsInstance(pdbx, PDBX)
    
    def test_exists(self):
        pdbx = PDBX.CreateFromGZIP(self.pdbx_5ou3_cif_fn)
        self.assertTrue(pdbx.exists('atom_site'))
        self.assertFalse(pdbx.exists('non_existent'))
    
    def test_getObj(self):
        pdbx = PDBX.CreateFromGZIP(self.pdbx_5ou3_cif_fn)
        entry = pdbx.getObj('entry')
        self.assertEqual(entry.getValue('id'), "5OU3")
        self.assertEqual(pdbx.getObj("exptl").getValue("method"), "X-RAY DIFFRACTION")
        with self.assertRaises(KeyError):
            pdbx.getObj('non_existent')

class TestPDBXReader5LH4(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")
        cls.pdbx = PDBX.CreateFromGZIP(cls.pdbx_5lh4_cif_fn)
    
    def test_getObjAsDF(self):
        struct_asym = self.pdbx.getObjAsDF('struct_asym')
        self.assertIsInstance(struct_asym, pandas.DataFrame)
        self.assertEqual(struct_asym.shape[0], 7)
        self.assertEqual(struct_asym.shape[1], 5)
        self.assertTrue(pandas.Series.all(struct_asym.id == [x for x in "ABCDEFG"]))
        self.assertTrue(pandas.Series.all(struct_asym.pdbx_blank_PDB_chainid_flag == ['N'] * 7))
        self.assertTrue(pandas.Series.all(struct_asym.pdbx_modified == ['N'] * 7))
        self.assertTrue(pandas.Series.all(struct_asym.entity_id == [x for x in "1223456"]))
        self.assertTrue(pandas.Series.all(struct_asym.details == ['?'] * 7))

    def test_getObjAsDFNonLoop(self):
        """ Can also get a DataFrame of non _loop object """
        struct_keywords = self.pdbx.getObjAsDF('struct_keywords')
        self.assertIsInstance(struct_keywords, pandas.DataFrame)
        self.assertEqual(struct_keywords.shape[0], 1)
        self.assertEqual(struct_keywords.shape[1], 3)
        self.assertEqual(struct_keywords.entry_id[0], "5LH4")
        self.assertEqual(struct_keywords.text[0], 'S1 protease, Complex, Inhibitor, pancreatitis, hydrolase')
        self.assertEqual(struct_keywords.pdbx_keywords[0], "HYDROLASE")
        
    def test_getObjAsDFKeyError(self):
        with self.assertRaises(KeyError):
            self.pdbx.getObjAsDF('non_existent')
    
    def test_getAtomSites(self):
        atom_sites = self.pdbx.getAtomSites()
        self.assertIsInstance(atom_sites, PDBXAtomSites)

class TestPDBXAtomSites(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")
        cls.pdbx = PDBX.CreateFromGZIP(cls.pdbx_5lh4_cif_fn)
        cls.atom_sites = cls.pdbx.getAtomSites()
    
    def test_Instance(self):
        self.assertIsInstance(self.atom_sites, PDBXAtomSites)
    
    def test_getResidueMmCIFPosition(self):
        res = self.atom_sites.getResidueMmCIFPosition("A", "170")
        self.assertIsInstance(res, PDBXResidueAtomSites)
        resEntity = self.atom_sites.getResidueMmCIFPosition("A", "170", entity="1")
        self.assertIsInstance(resEntity, PDBXResidueAtomSites)
    
    def test_getResidueAuthorPosition(self):
        res = self.atom_sites.getResidueAuthorPosition("A", "188")
        self.assertIsInstance(res, PDBXResidueAtomSites)
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        self.assertIsInstance(resInscode, PDBXResidueAtomSites)
    
    def test_getResidueAuthorPositionInsCode(self):
        """ Test with different ins_code values """
        res = self.atom_sites.getResidueAuthorPosition("A", "188")
        res1 = self.atom_sites.getResidueAuthorPosition("A", "188", None)
        self.assertEqual(res, res1)
        res2 = self.atom_sites.getResidueAuthorPosition("A", "188", "") # OST
        self.assertEqual(res, res2)
        res3 = self.atom_sites.getResidueAuthorPosition("A", "188", " ") # XML
        self.assertEqual(res, res3)
        res4 = self.atom_sites.getResidueAuthorPosition("A", "188", "?") # mmCIF
        self.assertEqual(res, res4)
                
    def test_getResidueMmCIFPositionNonExistent(self):
        # Wrong residue number
        with self.assertRaises(ResidueNotFound):
            self.atom_sites.getResidueMmCIFPosition("A", "1700")
        # Wrong entity
        with self.assertRaises(ResidueNotFound):
            self.atom_sites.getResidueMmCIFPosition("A", "170", entity = "2")
    
    def test_getResidueAuthorPositionNonExistent(self):
        # Wrong residue number
        with self.assertRaises(ResidueNotFound):
            self.atom_sites.getResidueAuthorPosition("A", "1700")
        # Wrong entity
        with self.assertRaises(ResidueNotFound):
            self.atom_sites.getResidueAuthorPosition("A", "188", ins_code = "B")
        
    def test_equality(self):
        res169 = self.atom_sites.getResidueMmCIFPosition("A", "169")
        res169b = self.atom_sites.getResidueMmCIFPosition("A", "169")
        res170 = self.atom_sites.getResidueMmCIFPosition("A", "170")
        self.assertTrue(res169 == res169b)
        self.assertFalse(res169 == res170)
        self.assertFalse(res169 != res169b)
        self.assertTrue(res169 != res170)
    
    def test_getResidueMmCIFPositionCorrectResidue(self):
        res = self.atom_sites.getResidueMmCIFPosition("A", "170")
        resEntity = self.atom_sites.getResidueMmCIFPosition("A", "170", entity="1")
        self.assertEqual(res, resEntity)
        self.assertEqual(res.getEntity(), "1")
        self.assertEqual(res.getMmCIFChain(), "A")
        self.assertEqual(res.getMmCIFResNum(), "170")
    
    def test_getResidueAuthPositionCorrectResidue(self):
        res = self.atom_sites.getResidueAuthorPosition("A", "188")
        self.assertEqual(res.getInsertionCode(), "?")
        self.assertEqual(res.getAuthChain(), "A")
        self.assertEqual(res.getAuthResNum(), "188")
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        self.assertEqual(resInscode.getInsertionCode(), "A")
        self.assertEqual(resInscode.getAuthChain(), "A")
        self.assertEqual(resInscode.getAuthResNum(), "188")

class TestPDBXResidueAtomSites(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5lh4_cif_fn = os.path.join(cls.data_dir, "5lh4.cif.gz")
        cls.pdbx = PDBX.CreateFromGZIP(cls.pdbx_5lh4_cif_fn)
        cls.atom_sites = cls.pdbx.getAtomSites()
    
    def test_listAlt(self):
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        self.assertEqual(resInscode.listAlt(), {'A', 'B'})
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="?")
        self.assertEqual(resInscode.listAlt(), {'.'})
        
    def test_getAlt(self):
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        self.assertTrue(resInscode.hasAlt())
        altA = resInscode.getAlt("A")
        self.assertIsInstance(altA, PDBXResidueAtomSites)
        self.assertFalse(altA.hasAlt())
        self.assertEqual(altA.listAlt(), {'A'})
        self.assertEqual(altA.getResName(), 'LYS')
        self.assertEqual(altA.getEntity(), '1')
        self.assertEqual(altA.getMmCIFChain(), 'A')
        self.assertEqual(altA.getAuthChain(), 'A')
        self.assertEqual(altA.getMmCIFResNum(), '170')
        self.assertEqual(altA.getOSTResNum(), '170')
        self.assertEqual(altA.getAuthResNum(), '188')
        self.assertEqual(altA.getInsertionCode(), 'A')
        # Make sure we can't getAlt(None) etc.
        with self.assertRaises(KeyError):
            resInscode.getAlt(None)
        with self.assertRaises(KeyError):
            resInscode.getAlt(" ")
        with self.assertRaises(KeyError):
            resInscode.getAlt(".")
    
    def test_iter(self):
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        alts = list(resInscode.iter())
        self.assertEqual(len(alts), 2)

    def test_NoAlt(self):
        """ A:GLY188 has no alternative configuration. Make sure it is reported correctly """
        resNoAlt = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="?")
        self.assertFalse(resNoAlt.hasAlt())
        self.assertEqual(resNoAlt.listAlt(), {'.'})
        self.assertEqual(resNoAlt.getResName(), 'GLY')
        self.assertEqual(resNoAlt.getAlt(None).getResName(), "GLY")
        self.assertEqual(resNoAlt.getAlt(".").getResName(), "GLY")
        self.assertEqual(resNoAlt.getAlt(" ").getResName(), "GLY")
        # Cannot get non-existing A
        with self.assertRaises(KeyError):
            resNoAlt.getAlt("A")
    
    def test_getAltKeyError(self):
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="?")
        self.assertFalse(resInscode.hasAlt())
        with self.assertRaises(KeyError):
            resInscode.getAlt('A')
    
    def test_ID(self):
        resInscode = self.atom_sites.getResidueAuthorPosition("A", "188", ins_code="A")
        self.assertEqual(resInscode.getAuthID(), "A:LYS.188A")
        self.assertEqual(resInscode.getMachineAuthID(), "A:LYS:188:A")
        self.assertEqual(resInscode.getMachineAuthID("."), "A.LYS.188.A")
        self.assertEqual(resInscode.getMmCIFID(), "A:LYS.170")
        self.assertEqual(resInscode.getMachineMmCIFID(), "A:LYS:170")
        self.assertEqual(resInscode.getMachineMmCIFID("."), "A.LYS.170")
        
    
if __name__ == '__main__':
    unittest.main()
