import os
import unittest

import pandas
import ost
from ost import io, conop, GetSharedDataPath

from ..Mapping import *
from ..PDBXReader import *
from ..Residue import *
from ..Validation import *
from ..ValidationFactory import *
from ..XML import *


class TestPDBValidationInit(unittest.TestCase):

    def setUp(self):
        # We need a working compound lib
        compound_lib = conop.GetDefaultLib()
        if compound_lib is None: # not running through ost
            # Try ost's shared path
            compound_lib_path = os.path.join(GetSharedDataPath(),
                                             'compounds.chemlib')
            if os.path.exists(compound_lib_path):
                compound_lib = conop.CompoundLib.Load(compound_lib_path)
                conop.SetDefaultLib(compound_lib)
            # Try environment variable COMPLIB_PATH
            else:
                compound_lib = conop.CompoundLib.Load(
                    os.environ["COMPLIB_PATH"])
                conop.SetDefaultLib(compound_lib)
        if compound_lib is None:
            raise RuntimeError("Compound Lib not available")
        # Ensure we use the rule based processor
        io.profiles['DEFAULT'].processor = conop.RuleBasedProcessor(
            compound_lib)

    @classmethod
    def setUpClass(cls):
        data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")
        cls.pdbx_5lh4_validation_fn = os.path.join(data_dir, "5lh4_validation.xml.gz")
        cls.pdbx_5lh4_cif_fn = os.path.join(data_dir, "5lh4.cif.gz")
        cls.pdbx_5ou3_validation_fn = os.path.join(data_dir, "5ou3_validation.xml.gz")
        cls.pdbx_5ou3_cif_fn = os.path.join(data_dir, "5ou3.cif.gz")
        cls.pdbx_5x1g_validation_fn = os.path.join(data_dir, "5x1g_validation.xml.gz")
        cls.pdbx_5x1g_cif_fn = os.path.join(data_dir, "5x1g.cif.gz")

    def test_init(self):
        pdbx_5lh4_factory = ValidationFactory(self.pdbx_5lh4_validation_fn, self.pdbx_5lh4_cif_fn)
        self.assertTrue(hasattr(pdbx_5lh4_factory, "getValidation"))
        pdbx_5ou3_factory = ValidationFactory(self.pdbx_5ou3_validation_fn, self.pdbx_5ou3_cif_fn)
        self.assertTrue(hasattr(pdbx_5ou3_factory, "getValidation"))
        pdbx_5x1g_factory = ValidationFactory(self.pdbx_5x1g_validation_fn, self.pdbx_5x1g_cif_fn)
        self.assertTrue(hasattr(pdbx_5x1g_factory, "getValidation"))

    def test_init_with_conop(self):
        conop = ost.conop.GetDefaultLib()
        pdbx_5lh4_factory = ValidationFactory(self.pdbx_5lh4_validation_fn, self.pdbx_5lh4_cif_fn, conop)
        self.assertTrue(hasattr(pdbx_5lh4_factory, "getValidation"))
        pdbx_5ou3_factory = ValidationFactory(self.pdbx_5ou3_validation_fn, self.pdbx_5ou3_cif_fn, conop)
        self.assertTrue(hasattr(pdbx_5ou3_factory, "getValidation"))
        pdbx_5x1g_factory = ValidationFactory(self.pdbx_5x1g_validation_fn, self.pdbx_5x1g_cif_fn, conop)
        self.assertTrue(hasattr(pdbx_5x1g_factory, "getValidation"))

    def test_getValidation(self):
        pdbx_5lh4_factory = ValidationFactory(self.pdbx_5lh4_validation_fn, self.pdbx_5lh4_cif_fn)
        validation_5lh4 = pdbx_5lh4_factory.getValidation()
        self.assertIsInstance(validation_5lh4, PDBValidation)
        pdbx_5ou3_factory = ValidationFactory(self.pdbx_5ou3_validation_fn, self.pdbx_5ou3_cif_fn)
        validation_5ou3 = pdbx_5ou3_factory.getValidation()
        self.assertIsInstance(validation_5ou3, PDBValidation)
        pdbx_5x1g_factory = ValidationFactory(self.pdbx_5x1g_validation_fn, self.pdbx_5x1g_cif_fn)
        validation_5x1g = pdbx_5x1g_factory.getValidation()
        self.assertIsInstance(validation_5x1g, PDBValidation)

    def test_PDBIDConsistency(self):
        """ Ensure that XML and mmCIF PDB IDs match at the time instantiation"""
        with self.assertRaises(Exception):
            ValidationFactory(self.pdbx_5lh4_validation_fn, self.pdbx_5ou3_cif_fn)


if __name__ == '__main__':
    unittest.main()
