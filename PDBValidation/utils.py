def elementToDict(elem):
    """
    Convert an lxml.etree.Element node into a dict.
    """
    d = {}
    for key, value in elem.items():
        d[key] = value
    return d

def elementToDictRecursive(elem):
    """
    Convert an lxml.etree.Element node into a dict recursively including the child elements.
    """
    d = {}
    for key, value in elem.items():
        key = elem.tag + '.' + key
        d[key] = value
    for child in elem.iterchildren():
        child_dict = elementToDictRecursive(child)
        for key, value in child_dict.items():
            key = elem.tag + '.' + key
            d.setdefault(key, []).append(value)
    return d

def createLigandMmCIFIDWithPDBID(pdbid, chain, resname, resnum, altcode = None):
    return pdbid + "_" + createLigandMmCIFID(chain, resname, resnum, altcode)

def createLigandOSTIDWithPDBID(pdbid, chain, resname, resnum, altcode = None):
    return pdbid + "_" + createLigandOSTID(chain, resname, resnum, altcode)

def createLigandAuthIDWithPDBID(pdbid, chain, resname, resnum, inscode = "", altcode = None):
    return pdbid + "_" + createLigandAuthID(chain, resname, resnum, inscode, altcode)

def createLigandMmCIFID(chain, resname, resnum, altcode = None):
    """ Create a ligand ID in the form A:GLN.188. If altcode B is given: A:GLN.188-B"""
    
    ligand_id = "{chain}:{resname}.{resnum}".format(
        chain=chain, resname=resname, resnum=resnum)
    if altcode is not None and altcode != " ": # alternative configurations:
        ligand_id += "-" + altcode
    return ligand_id

def createLigandMachineMmCIFID(chain, resname, resnum, altcode = None, sep=":"):
    """ Create a ligand ID in machine-friendly format, for instance:
    A:GLN:188:A. If altcode B is given: A:GLN:188:A:B"""
    
    ligand_id = "{chain}{sep}{resname}{sep}{resnum}".format(
        chain=chain, resname=resname, resnum=resnum, sep=sep)
    if altcode is not None and altcode != " ": # alternative configurations:
        ligand_id += sep + altcode
    return ligand_id

def createLigandAuthID(chain, resname, resnum, inscode, altcode = None):
    """ Create a ligand ID in the form A:GLN.188A. If altcode B is given: A:GLN.188A-B"""
    
    if inscode is None or inscode == " " or inscode == "?": # normalize insertion code
        inscode = ""
    ligand_id = "{chain}:{resname}.{resnum}{inscode}".format(
        chain=chain, resname=resname, resnum=resnum,
        inscode=inscode
        )
    if altcode is not None and altcode != " ": # alternative configurations:
        ligand_id += "-" + altcode
    return ligand_id

def createLigandMachineAuthID(chain, resname, resnum, inscode, altcode = None, sep=":"):
    """ Create a ligand ID in machine-friendly format, for instance:
    A:GLN:188:A. If altcode B is given: A:GLN:188:A:B"""
    
    if inscode is None or inscode == " " or inscode == "?": # normalizeinsertion code
        inscode = ""
    ligand_id = "{chain}{sep}{resname}{sep}{resnum}{sep}{inscode}".format(
        chain=chain, resname=resname, resnum=resnum,
        inscode=inscode, sep=sep
        )
    if altcode is not None and altcode != " ": # alternative configurations:
        ligand_id += sep + altcode
    return ligand_id