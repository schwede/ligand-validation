import os

from numpy import log as ln
from numpy import mean, median
import pandas as pd

import ost
from ost.io import LoadMMCIF
from ost import conop

from .Mapping import PDBMapper
from .PDBXReader import PDBX
from .Residue import *
from .XML import PDBValidationXML, ModelledSubgroupNotFound

class PDBValidation():
    """Parses and stores all information about the a PDB Validation data."""
    
    unknown_resnames = ["UNK", "UNL", "UNX"]
    ignore_resnames = ["HOH", "UNK", "UNL", "UNX"]
    
    def __init__(self, xml, ent, ost_fault_tolerant, seqres, mmcif_info, pdbx, atom_sites, reflns_shell,
                 non_polymer_ligands, mapper, compound_lib, pdbid):
        self.xml = xml
        self.ent = ent
        self.ost_fault_tolerant = ost_fault_tolerant
        self.seqres = seqres
        self.mmcif_info = mmcif_info
        self.pdbx = pdbx
        self.atom_sites = atom_sites
        self.reflns_shell = reflns_shell
        self.non_polymer_ligands = non_polymer_ligands
        self.mapper = mapper
        self.compound_lib = compound_lib
        self.pdbid = pdbid

        # Store pre-calculated stuff
        self._mean_structure_b_factor = None
        self._median_structure_b_factor = None
    
    def needOSTFaultTolerant(self):
        return self.ost_fault_tolerant
    
    def getPDBId(self):
        return self.pdbid
    
    def getCompoundLib(self):
        return self.compound_lib
    
    def getPdbx(self):
        return self.pdbx
    
    def getReflnsShell(self):
        if self.reflns_shell is False:
            return None
        return self.reflns_shell

    def getMapper(self):
        return self.mapper
    
    def getMethod(self):
        return self.getPdbx().getObj("exptl").getValue("method")
    
    def getResolution(self):
        res_str = self.getPdbx().getObj("refine").getValue("ls_d_res_high")
        try:
            return float(res_str)
        except ValueError:
            return float("NaN")
        
    def getReflectionsResolution(self):
        res_str = self.getPdbx().getObj("reflns").getValue("d_resolution_high")
        try:
            return float(res_str)
        except ValueError:
            return float("NaN")
    
    def getMeanIOverSigIObs(self):
        reflns_shell = self.getReflnsShell()
        if reflns_shell is None:
            return None
        try:
            idxmin = pd.to_numeric(reflns_shell["d_res_high"]).idxmin()
        except ValueError:
            # d_res_high sometimes missing with . instead of value
            return None
        return self.getReflnsShell()['meanI_over_sigI_obs'][idxmin]
    
    def hasNonPolymerLigands(self):
        return self.getPdbx().exists("pdbx_entity_nonpoly")
    
    def calcMolProbityOverallScore(self):
        """ Calculate the Overall MolProbity score as given in
        http://kinemage.biochem.duke.edu/suppinfo/CASP8/methods.html
        
        See also source code of cmdline/molparser.py:calcMPscore
        https://github.com/rlabduke/MolProbity/blob/6e7512e85bdea23f7ffb16e606d1f9a0abf6e5d4/cmdline/molparser.py#L662
        """
        entry = self.xml.getEntry()
        
        # lxml.Element.get() returns None when the attribute doesn't exist
        # instead of raising a KeyError. This means that we
        # get a meaningless TypeError instead. Catch this and
        # return None as well
        try:
            clashscore = float(entry.get("clashscore"))
            rota_out = float(entry.get("percent-rota-outliers"))
            rama_iffy = float(entry.get("percent-rama-outliers"))
        except TypeError:
            return None
        MPscore = 0.426 *ln(1+clashscore) + 0.33 *ln(1+max(0, rota_out-1)) + 0.25 *ln(1+max(0, rama_iffy-2)) + 0.5
        return MPscore

    def calcMedianStructureBFactor(self):
        if self._median_structure_b_factor is None:
            self._median_structure_b_factor = median([atom.b_factor for atom in self.getEntity().atoms])
        return self._median_structure_b_factor

    def calcMeanStructureBFactor(self):
        if self._mean_structure_b_factor is None:
            self._mean_structure_b_factor = mean([atom.b_factor for atom in self.getEntity().atoms])
        return self._mean_structure_b_factor
    
    def isNonPolymerLigand(self, entity, resname):
        """ Test whether the residue given as entity and residue name corresponds to a non-polymer ligand.
        Criteria: resname and entity match an entry in pdbx_entity_nonpoly
        In addition, the compound must not be in the ignored resnames self.ignore_resnames
        
        :param entity: the entity_id
        :type entity: :str
        :param resname: the residue name (comp_id)
        :type resname: :str
        
        :return :bool
        """
        
        if self.hasNonPolymerLigands():
            non_poly = self.getNonPolymerLigands()
            
            test_series = (non_poly['entity_id'] == entity) & \
                (non_poly['comp_id'] == resname) & \
                (~ non_poly['comp_id'].isin(self.ignore_resnames))
            
            return test_series.any()
        else:
            return False
    
    def isModelledSubgroupNonPolymerLigand(self, modelled_subgroup):
        """ Test whether the given <ModelledSubgroup> corresponds to a non-polymer ligand.
        Criteria: resname and entity match an entry in pdbx_entity_nonpoly
        In addition, the compound must not be in the ignored resnames self.ignore_resnames
        
        :param modelled_subgroup: the <ModelledSubgroup>
        :type modelled_subgroup: :lxml.Element
        
        :return :bool
        """
        return self.isNonPolymerLigand(modelled_subgroup.get('ent'), modelled_subgroup.get('resname'))
    
    def findModelledSubgroupNonPolymerLigands(self):
        """ Generator over ModelledSubgroup elements of non-polymer ligands """
        if not self.hasNonPolymerLigands():
            return # Nothing to do if no polymer ligand
        for modelled_subgroup in self.xml.findModelledSubgroups():
            if self.isModelledSubgroupNonPolymerLigand(modelled_subgroup):
                yield modelled_subgroup
    
    def findNonPolymerLigands(self):
        """ Generator over non-polymer ligands.
        
        Yields Residue subclasses instances. If a ModelledSubgroup cannot be found
        for a Residue, yields (not raises!) the ModelledSubgroupNotFound exception.
        This means that you need to check if you received a ModelledSubgroupNotFound
        as the first step of the loop. For instance:
        
        for ligand in doc.findNonPolymerLigands():
            if isinstance(ligand, ModelledSubgroupNotFound):
                ... mark the ligand as bad ...
        
         """
        if not self.hasNonPolymerLigands():
            return # Nothing to do if no polymer ligand
        from .Residue import Ligand # TODO: break recursive dependency 
        nonpoly = self.getNonPolymerLigands()
        atom_sites = self.getAtomSites()
        # Filter atom site entities that are non polymer ligand
        atoms = atom_sites[atom_sites['label_entity_id'].isin(nonpoly.entity_id)]
        for residue, residue_atoms in atoms.groupby( [ "label_asym_id", "label_entity_id", "label_seq_id"] ):
            if residue_atoms.label_comp_id.iloc[0] in PDBValidation.ignore_resnames:
                continue
            chain, entity, res_num = residue
            try:
                yield Ligand.CreateFromMmCIFPosition(chain, res_num, entity, self)
            except ModelledSubgroupNotFound as e:
                yield e
    
    def failedReproduceRValues(self):
        """ Tests if the reproduction of R values by PDB failed completely
        
        According to Monica @ PDB Validation Mailing List:
        
        "There are cases when we are unable to reproduce the reported R values
        and as a result, section 6 is empty.  This is the expected behavior."
        
        As a result the whole entry doesn't have a single RSCC or RSR value
        set. In that case the function will return True.
        If a single RSR or RSCC value is found, it will return False."""

        for subgroup in self.getValidationXML().findModelledSubgroups():
            if 'rsr' in list(subgroup.keys()) or 'rscc' in list(subgroup.keys()):
                return False
        return True
    
    def getSeqRes(self):
        return self.seqres
    
    def getMMCIFInfo(self):
        return self.mmcif_info
    
    def getEntity(self):
        return self.ent
    
    def getValidationXML(self):
        return self.xml
    
    def getAtomSites(self):
        """ Returns a PDBXAtomSites object containing complete atom_sites
        with all atoms of the mmCIF file"""
        return self.atom_sites
    
    def getPdbxResidueAuthorPosition(self, chain, res_num, ins_code):
        """ Get the residue with the author coordinates.
        
        :param chain: chain name
        :type chain: :str
        :param res_num: residue number
        :type res_num: int
        :param ins_code: insertion code
        :type ins_code: str
        :returns: :class:`PDBXResidueAtomSites`
        """
        return self.getAtomSites().getResidueAuthorPosition(chain, res_num, ins_code)
    
    def getPdbxResidueMmCIFPosition(self, chain, res_num, entity):
        """ Get the residue with the author coordinates.
        
        :param chain: chain name
        :type chain: :str
        :param res_num: residue number
        :type res_num: int
        :param entity: mmCIF entity number
        :type entity: str
        :returns: :class:`PDBXResidueAtomSites`
        """
        return self.getAtomSites().getResidueMmCIFPosition(chain, res_num, entity)
    
    def getNonPolymerLigands(self):
        """ Get the content of mmCIF pdbx_entity_nonpoly block as a pandas DataFrame """
        return self.non_polymer_ligands
    
    def countAtoms(self):
        return self.getAtomSites().shape[0]
    
    def getOSTLigand(self, chain, res_name, res_num, ins_code = ""):
        chain_handle = self.getEntity().FindChain(chain)
        if chain_handle.IsValid():
            res_handle = chain_handle.FindResidue(ost.mol.ResNum(int(res_num), ins_code))
            if res_handle.IsValid():
                if res_handle.name == res_name:
                    return res_handle
                else:
                    raise ValueError("Invalid residue {res_num} name {res_name_obtained} on chain {chain}: expected {res_name_exp}".format(
                        res_num=res_num, res_name_obtained=res_handle.name, chain=chain, res_name_exp=res_name))
            else:
                raise ValueError("Invalid residue {0} on chain {1}".format(res_num, chain))
        else:
            raise ValueError("Invalid chain {0}".format(chain))
