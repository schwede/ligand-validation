import collections
from itertools import chain

from lxml import etree
from .utils import *

class ModelledSubgroupNotFound(ValueError):
    def __init__(self, xpath, pdbid, chain, resnum, inscode, altcode = None):
        
        # Create custom message
        message = pdbid + ": " + xpath
        super(ValueError, self).__init__(message)
        
        # Save residue for further inspection
        self.xpath = xpath
        self.pdbid = pdbid
        self.chain = chain
        self.resnum = resnum
        self.inscode = inscode
        self.altcode = altcode
    

class PDBValidationXML():
    """ Parse and store the validation XML data """
    def __init__(self, validation_path):
        """ Create a new PDBValidation object given the path to the validation file
        
        :param validation_path: path to the *_validation.xml.gz file
        :type validation_path: :str
        """
        self.doc = etree.parse(validation_path)
        
    def getEntry(self):
        """ Get the <Entry> element."""
        entries = self.doc.findall("Entry")
        if len(entries) > 1:
            raise ValueError("More than one <Entry> in this document")
        elif len(entries) == 0:
            raise ValueError("No <Entry> in this document")
        entry = entries[0]
#       Auto-convert attributes to useful types (ie numbers, lists etc)
#       Unfortunately lxml doesn't seem to support non-str types as attributes :(
#         float_cols = ['DCC_R', 'DCC_Rfree', 'DataAnisotropy', 'DataCompleteness', 'EDS_R',
#                       'EDS_resolution', 'EDS_resolution_low', 'Fo_Fc_correlation',
#                       'PDB-R', 'PDB-Rfree', 'PDB-resolution', 'PDB-resolution-low',
#                       'TwinL', 'TwinL2', 'percent-RSRZ-outliers', 'percent-free-reflections',
#                       'percent-rama-outliers', 'percent-rota-outliers',
#                       'relative-percentile-DCC_Rfree', 'relative-percentile-clashscore',
#                       'relative-percentile-percent-RSRZ-outliers',
#                       'relative-percentile-percent-rama-outliers',
#                       'relative-percentile-percent-rota-outliers', 'WilsonBestimate'
#                       'absolute-percentile-DCC_Rfree', 'absolute-percentile-clashscore',
#                       'absolute-percentile-percent-RSRZ-outliers',
#                       'absolute-percentile-percent-rama-outliers',
#                       'absolute-percentile-percent-rota-outliers', 'angles_rmsz',
#                       'babinet_b', 'babinet_k', 'bonds_rmsz', 'bulk_solvent_b',
#                       'bulk_solvent_k', 'clashscore',
#                       'high-resol-relative-percentile-DCC_Rfree',
#                       'high-resol-relative-percentile-clashscore',
#                       'high-resol-relative-percentile-percent-RSRZ-outliers',
#                       'high-resol-relative-percentile-percent-rama-outliers',
#                       'high-resol-relative-percentile-percent-rota-outliers',
#                       'low-resol-relative-percentile-DCC_Rfree',
#                       'low-resol-relative-percentile-clashscore',
#                       'low-resol-relative-percentile-percent-RSRZ-outliers',
#                       'low-resol-relative-percentile-percent-rama-outliers',
#                       'low-resol-relative-percentile-percent-rota-outliers']
#         int_cols = ['num-H-reduce', 'num-free-reflections', 'numMillerIndices',
#                     'numPDBids-absolute-percentile-DCC_Rfree',
#                     'numPDBids-absolute-percentile-clashscore',
#                     'numPDBids-absolute-percentile-percent-RSRZ-outliers',
#                     'numPDBids-absolute-percentile-percent-rama-outliers',
#                     'numPDBids-absolute-percentile-percent-rota-outliers',
#                     'numPDBids-relative-percentile-DCC_Rfree',
#                     'numPDBids-relative-percentile-clashscore',
#                     'numPDBids-relative-percentile-percent-RSRZ-outliers',
#                     'numPDBids-relative-percentile-percent-rama-outliers',
#                     'numPDBids-relative-percentile-percent-rota-outliers',
#                     'num_angles_rmsz', 'num_bonds_rmsz', 'acentric_outliers',
#                     'centric_outliers', 'protein-DNA-RNA-entities']
#         for col in float_cols:
#             try:
#                 value = entry.get(col)
#             except KeyError:
#                 continue
#             entry.set(col, float(value))
#         for col in int_cols:
#             try:
#                 value = entry.get(col)
#             except KeyError:
#                 continue
#             entry.set(col, int(value))
#         try:
#             entry.set('XMLcreationDate',
#                       pandas.to_datetime(entry.get('XMLcreationDate'), infer_datetime_format=True))
#         except UnknownTimezoneWarning:
#             pass
        return entry
    
    def getModelledSubgroup(self, entity, chain, resname, resnum, model, icode = " ", altcode = " "):
        """ Get a single <ModelledSubgroup> lxml.etree.Element.
        All the values are given in the "author" annotation
        
        :param entity: entity id
        :type entity: :str
        :param chain: chain name
        :type chain: :str
        :param resname: residue name or None to allow any residue
        :type resname: :str
        :param resnum: residue number
        :type resnum: :str
        :param model: model id
        :type model: :str
        :param icode: insertion code, defaults to a space (no insertion code). '?' (missing value in cif file), '' or None will be converted to ' '.
        :type icode: :str
        :param altcode: alternate configuration code, defaults to a space (no alternate). '.' (missing value in cif file) will be converted to ' '.
        :type altcode: :str

        :returns: lxml.etree.Element
        """
        if icode is None or icode == '?':
            icode = ' '
        if altcode is None or altcode == '.':
            altcode = ' '
        # Strip leading 0 in
        xpath = ".//ModelledSubgroup" \
                    "[@chain='{chain}']" \
                    "[@resnum='{resnum}']" \
                    "[@icode='{icode}']" \
                    "[@altcode='{altcode}']" \
                    "[@model='{model}']".format(
                        chain = chain,
                        resnum = resnum,
                        icode = icode,
                        model = model,
                        altcode = altcode)
        if resname is not None:
            xpath += "[@resname='{resname}']".format(resname=resname)
        if entity is not None:
            xpath += "[@ent='{entity}']".format(entity = entity)
        result = self.doc.findall(xpath)
        
        if len(result) > 1:
            raise RuntimeError("Multiple <ModelledSubgroup> found: %s"%xpath)
        if len(result) == 0:
            #if resnum.startswith("0"):
            #    # 6EKC has residue number 067 and others with leading 0. Try to strip:
            #    # Actually it also doesn't have residue number 67, reported to RCSB
            #    return self.getModelledSubgroup(entity, chain, resname, resnum.lstrip("0"), model, icode, altcode)
            pdbid = self.doc.findall("Entry")[0].get("pdbid")
            raise ModelledSubgroupNotFound(xpath, pdbid, chain, resnum, icode, altcode)
        
        return ModelledSubgroup(result[0])
    
    def getModelledSubgroups(self, entity, chain, resnum, model, icode = " "):
        """ Get the <ModelledSubgroup> `lxml.etree.Element`s for the given ligand.
        This includes all the alternative configuration entries as well and returns a list.
        
        :param entity: entity id
        :type entity: :str
        :param chain: chain name
        :type chain: :str
        :param resnum: residue number
        :type resnum: :str
        :param model: model id
        :type model: :str
        :param icode: insertion code, defaults to a space (no insertion code). '?' (missing value in cif file) will be converted to ' '.
        :type icode: :str

        :returns: lxml.etree.Element
        """
        if icode is None or icode == "" or icode == '?':
            icode = ' '
        # Strip leading 0 in
        xpath = ".//ModelledSubgroup" \
                    "[@chain='{chain}']" \
                    "[@resnum='{resnum}']" \
                    "[@icode='{icode}']" \
                    "[@model='{model}']".format(
                        chain = chain,
                        resnum = resnum,
                        icode = icode,
                        model = model)
        if entity is not None:
            xpath += "[@ent='{entity}']".format(entity = entity)
        result = self.doc.findall(xpath)
        
        if len(result) == 0:
            #if resnum.startswith("0"):
            #    # 6EKC has residue number 067 and others with leading 0. Try to strip:
            #    # Actually it also doesn't have residue number 67, reported to RCSB
            #    return self.getModelledSubgroup(entity, chain, resname, resnum.lstrip("0"), model, icode, altcode)
            pdbid = self.doc.findall("Entry")[0].get("pdbid")
            raise ModelledSubgroupNotFound(xpath, pdbid, chain, resnum, icode)
        
        return ModelledSubgroups(result)
        
    def findModelledSubgroups(self):
        """ Generator over all the ModelledSubgroup elements in the document"""
        for subgroup in self.doc.findall("ModelledSubgroup"):
            yield ModelledSubgroup(subgroup)


class ModelledSubgroups():
    """ Represents the <ModelledSubgroup> elements for the alternative
    configurations of a single residue.
    Holds all alternative configurations of the residue and provides
    convenience functions to list and access alternative configurations.
    """
    
    def __init__(self, list):
        self.subgroups = list
        # List all alternative configurations
        alt_configs = [x.get('altcode') for x in self.subgroups]
        self.alt_configs = set(alt_configs)
        assert len(alt_configs) == len(self.alt_configs), \
            "Duplicate alternative configurations found in %s" % self.subgroups

    def getAlt(self, alt_code = None):
        """ Returns the ModelledSubgroup of the given alternative configuration.
        Atoms with no alternative configuration are denoted with a space in the XML file.
        None, a dot or an empty string will be converted to such a space.
        
        The return type is a ModelledSubgroup object, which inherits from etree.Element.
        """
        if alt_code == '.' or alt_code is None or alt_code == '':
            alt_code = " "
        
        if alt_code not in self.alt_configs:
            ligand_id = createLigandAuthID(self.getAuthChain(), self.getResName(), self.getAuthResNum(), self.getInsertionCode(),
                                       alt_code)
            raise KeyError("No such alternative configuration {0}".format(ligand_id))
        
        for subgroup in self.subgroups:
            if subgroup.get('altcode') == alt_code:
                return ModelledSubgroup(subgroup)
        
        assert 0, "Code shouldn't be reachable" # The loop should always return!

    def hasAlt(self):
        """ Tests if this residue has alternative configurations. """
        return len(self.alt_configs) > 1
    
    def listAlt(self):
        """ Lists all alternative configurations."""
        return self.alt_configs
    
    def iter(self):
        for alt in self.listAlt():
            yield alt, self.getAlt(alt)
    
    def getResName(self):
        """ Residue name
        In case of heterogeneity of the alternative positions, the different
        names will be separated by a pipe character, such as SER|SEP """
        unique_resnames = list(collections.OrderedDict.fromkeys([x.get('resname') for x in self.subgroups]))
        return "|".join(unique_resnames)
    
    def getEntity(self):
        """ Entity """
        return self.subgroups[0].get('ent')
        
    def getAuthChain(self):
        """ Chain (author name) """
        return self.subgroups[0].get('chain')
            
    def getAuthResNum(self):
        """ Residue number (author position) """
        return self.subgroups[0].get('resnum')
    
    def getInsertionCode(self):
        """ Insertion code (author position) """
        return self.subgroups[0].get('icode')
    
    def getModel(self):
        """ Model number """
        return self.subgroups[0].get('model')



class ModelledSubgroup():
    """ Represents the <ModelledSubgroup> elements for one single 
    alternative configuration of a single residue.
    """
    def __init__(self, subgroup):
        self.subgroup = subgroup
        self.tag = subgroup.tag

    def get(self, name):
        return self.subgroup.get(name)

    def items(self):
        return self.subgroup.items()

    def findall(self, path, namespaces=None):
        return self.subgroup.findall(path, namespaces)

    def iterchildren(self, tag=None, reversed=False, *tags):
        return self.subgroup.iterchildren(tag=tag, reversed=reversed, *tags)
    
    def keys(self):
        return self.subgroup.keys()
        
    def getAltCode(self, alt_code = None):
        """ Alternative configuration of this residue.
        Not to be confused with ModelledSubgroup.getAlt which returns a whole
        ModelledSubgroup.
        """
        alt_code = self.get('altcode')
        if alt_code == ' ':
            alt_code = None
        return alt_code
    
    def getResName(self):
        """ Residue name """
        return self.get('resname')
    
    def getEntity(self):
        """ Entity """
        return self.get('ent')
        
    def getAuthChain(self):
        """ Chain (author name) """
        return self.get('chain')
            
    def getAuthResNum(self):
        """ Residue number (author position) """
        return self.get('resnum')
    
    def getInsertionCode(self):
        """ Insertion code (author position) """
        icode = self.get('icode')
        if icode == " ":
            icode = None
        return icode
    
    def getModel(self):
        """ Model number """
        return self.get('model')

    def findClashes(self):
        """ Return an generator of clash and symmetry clash (symm-clash) elements of this subgroup"""
        return chain(self.iterchildren('clash'), self.iterchildren('symm-clash'))

    def getClashAtomNames(self):
        """ Return a set containing the names of atoms of this ligand involved in clashes. The set is empty
        if the ligand has no clash"""
        atoms = set()
        for elem in self.findClashes():
            atoms.add(elem.get("atom"))
        return atoms