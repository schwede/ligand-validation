import collections
import gzip

import pandas as pd
import ost
from mmcif.io.PdbxReader import PdbxReader

from .utils import *

class ResidueNotFound(ValueError):
    pass

class PDBXAtomSites(pd.DataFrame):
    """ Represents PDBX atom_sites mmCIF data block """
    
    def __init__(self, df):
        """ Constructor
        
        :param df: a Pandas DataFrame of AtomSites, as returned by PDBX.getObjAsDF("atom_site")
        :type df: :class:`pandas.DataFrame`
        """
        pd.DataFrame.__init__(self, df)
        # Convert relevant columns to numeric
        self.occupancy = pd.to_numeric(self.occupancy)
        self.B_iso_or_equiv = pd.to_numeric(self.B_iso_or_equiv)
        assert (self.auth_comp_id == self.label_comp_id).all(), \
            "Author and mmCIF residue name differ (auth_comp_id vs label_comp_id)."

#     def getLigand(self, chain, res_name, res_num, ins_code, model, alt_code):
#         """ Get the ligand with the author coordinates"""
#         atoms = self.getAtomSites()
#         if ins_code == ' ':
#             ins_code = '?'
#         if alt_code == ' ':
#             alt_code = "."
#         ligand = atoms[(atoms['auth_asym_id'] == chain) & \
#               (atoms['auth_seq_id'] == res_num) & \
#               (atoms['auth_comp_id'] == res_name) & \
#               (atoms['pdbx_PDB_ins_code'] == ins_code) & \
#               # Always extract atoms without alternative configuration too.
#               # Sometimes only part of a residue has alternative atoms
#               # The next line is redundant when  label_alt_id = '.'
#               # but if it is A or B we also extract the constant part of the residue
#               ((atoms['label_alt_id'] == alt_code) | (atoms['label_alt_id'] == ".")) & \
#               (atoms['pdbx_PDB_model_num'] == model)]
#         if ligand.shape[0] < 1:
#             raise RuntimeError("LigandNotFound in {0}: {1}.{2}.{3}-{4}, {5}".format(
#                 self.validation_path, chain, res_name, res_num, ins_code, model))
#         return ligand
    
    def getResidueMmCIFPosition(self, chain, res_num, entity = None):
        """ Get the residue with the author coordinates.
        
        :param chain: chain name
        :type chain: :str
        :param res_num: residue number
        :type res_num: int
        :param entity: mmCIF entity number or None for any entity (should be safe, but
            the code still checks that we get a unique entity back)
        :type entity: :str
        :returns: :class:`PDBXResidueAtomSites`
        """
        atoms = self
        residue = atoms[(atoms['label_asym_id'] == chain) & \
              (atoms['label_seq_id'] == str(res_num))]
        
        if residue.shape[0] == 0 and res_num == 1:
            # If we're looking at a ligand, it's in its own chain
            # with resnum = '.' in the CIF file.
            # OST labels it as 1, so retry with . instead:
            ost.LogInfo("Retry with resnum = . instead of 1")
            return self.getResidueMmCIFPosition(chain, '.', entity)
        
        if entity is not None:
            residue = residue[residue['label_entity_id'] == entity]
        
        # Check residue found
        if residue.shape[0] == 0:
            raise ResidueNotFound("Residue not found: {0}.{1}-{2}".format(
                chain, res_num, entity))
            
        # Make sure we got a single entity (especially useful if
        # entity is None)
        assert len(residue["label_entity_id"].unique()) == 1, \
            "Multiple entities found: {0}.{1}: '{2}'".format(
                chain, res_num,
                ", ".join(residue["label_entity_id"].unique()))            
        

        # Check single resname
	# Disabled:
	# Alternative configurations can involve multiple residue
	# names, see 5za2 has B.64: SEP or SER
        #res_name = residue.iloc[0]["label_comp_id"]
        #assert (residue["label_comp_id"] == res_name).all(), \
        #    "Multiple residues found: {2}.{0}.{1}: {3}".format(
        #        chain, res_num, entity,
        #        ", ".join(residue["label_comp_id"].unique()))
        
        # Check single insertion code
        assert len(residue["pdbx_PDB_ins_code"].unique()) == 1, \
            "Multiple insertion codes found: {2}.{0}.{1}: {3}".format(
                chain, res_num, entity,
                ", ".join(residue["pdbx_PDB_ins_code"].unique()))
            
        return PDBXResidueAtomSites(residue)
        
    def getResidueAuthorPosition(self, chain, res_num, ins_code = None):
        """ Get the residue with the author coordinates.
        
        :param chain: chain name
        :type chain: :str
        :param res_num: residue number
        :type res_num: int
        :param ins_code: insertion code
        :type ins_code: str
        :returns: :class:`PDBXResidueAtomSites`
        """
        atoms = self
        if ins_code is None or ins_code == ' ' or ins_code == "":
            ins_code = '?'
        residue = atoms[(atoms['auth_asym_id'] == chain) & \
              (atoms['auth_seq_id'] == str(res_num)) & \
              (atoms['pdbx_PDB_ins_code'] == ins_code)]
        
        # Check residue found
        if residue.shape[0] == 0:
            raise ResidueNotFound("Residue not found: {0}.{1}-{2}".format(
                chain, res_num, ins_code))
        
        # Check single resname
	# Disabled:
	# Alternative configurations can involve multiple residue
	# names, see 5za2 has B.64: SEP or SER
        #res_name = residue.iloc[0]["auth_comp_id"]
        #assert (residue["auth_comp_id"] == res_name).all(), \
        #    "Multiple residues found: {0}.{1}-{2}: {3}".format(
        #        chain, res_num, ins_code,
        #        ", ".join(residue["auth_comp_id"].unique()))
        
        # Check single entity
        assert len(residue["label_entity_id"].unique()) == 1, \
            "Multiple entities found: {0}.{1}-{2}: {3}".format(
                chain, res_num, ins_code,
                ", ".join(residue["label_entity_id"].unique()))
                
        return PDBXResidueAtomSites(residue)


class PDBXResidueAtomSites(pd.DataFrame):
    """ Represents the mmCIF._atom_site entry for a single residue.
    Holds all alternative configurations of the residue and provides
    convenience functions to list and access alternative configurations.
    """
    
    # Declare new attributes so that Pandas recognizes them as such
    # rather than new columns.
    # See http://pandas.pydata.org/pandas-docs/stable/extending.html#define-original-properties
    _internal_names = pd.DataFrame._internal_names + ['alt_configs']
    _internal_names_set = set(_internal_names)
    
    def __init__(self, df):
        assert len(df['label_entity_id'].unique()) == 1, \
            "Non unique entity: %s" % ",".join(df['label_entity_id'].unique())
        #assert len(df['label_comp_id'].unique()) == 1, \
        #    "Non unique residue name: %s" % ",".join(df['label_comp_id'].unique())
        # List all alternative configurations
        #self.alt_configs = set(df['label_alt_id'].tolist())
        self.alt_configs = set(df['label_alt_id'])
        pd.DataFrame.__init__(self, df)
    
    def __eq__(self, other):
        """Equality comparison: is it the same residue"""
        if isinstance(self, other.__class__):
            return self.equals(other) # pandas.DataFrame.equals:  
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)

    def getAlt(self, alt_code = None):
        """ Returns the atom sites of the given alternative configuration.
        Atoms with no alternative configuration are denoted with a dot in the mmCIF
        file. None, a space or an empty string will be converted to such a dot.
        
        The return type is a pandas DataFrame with the following columns:
        ['group_PDB', 'id', 'type_symbol', 'label_atom_id', 'label_alt_id',
        'label_comp_id', 'label_asym_id', 'label_entity_id', 'label_seq_id',
        'pdbx_PDB_ins_code', 'Cartn_x', 'Cartn_y', 'Cartn_z', 'occupancy',
        'B_iso_or_equiv', 'pdbx_formal_charge', 'auth_seq_id', 'auth_comp_id',
        'auth_asym_id', 'auth_atom_id', 'pdbx_PDB_model_num']
        """
        if alt_code == ' ' or alt_code is None or alt_code == '':
            alt_code = "."
        
        if alt_code not in self.alt_configs:
            raise KeyError("No such alternative configuration {0}".format(alt_code))
        
        atoms = self
        # Always extract atoms without alternative configuration too.
        # Sometimes only part of a residue has alternative atoms
        # The next line is redundant when  label_alt_id = '.'
        # but if it is A or B we also extract the constant part of the residue
        config = PDBXResidueAtomSites(atoms[((atoms['label_alt_id'] == alt_code) | (atoms['label_alt_id'] == "."))])
        return config

    def hasAlt(self):
        """ Tests if this residue has alternative configurations. """
        return len(self.alt_configs) > 1
    
    def listAlt(self):
        """ Lists all alternative configurations."""
        return self.alt_configs
    
    def iter(self):
        for alt in self.listAlt():
            yield alt, self.getAlt(alt)
    
    def getResName(self):
        """ Residue name """
        unique_resnames = list(collections.OrderedDict.fromkeys(self.label_comp_id))
        return "|".join(unique_resnames)
    
    def getEntity(self):
        """ Entity """
        return self.iloc[0]['label_entity_id']
    
    def getMmCIFChain(self):
        """ Chain (mmCIF name) """
        return self.iloc[0]['label_asym_id']
    
    def getAuthChain(self):
        """ Chain (author name) """
        return self.iloc[0]['auth_asym_id']
    
    def getMmCIFResNum(self):
        """ Residue number (mmCIF position) """
        return self.iloc[0]['label_seq_id']
    
    def getOSTResNum(self):
        res_num = self.getMmCIFResNum()
        return 1 if res_num == "." else res_num
    
    def getAuthResNum(self):
        """ Residue number (author position) """
        return self.iloc[0]['auth_seq_id']
    
    def getInsertionCode(self):
        """ Insertion code (author position) """
        return self.iloc[0]['pdbx_PDB_ins_code']
    
    def getMmCIFID(self):
        """ Get a machine-readable ligand ID. Without Alt code.""" 
        return createLigandMmCIFID(self.getMmCIFChain(), self.getResName(),
                              self.getMmCIFResNum())
    
    def getMachineMmCIFID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code.""" 
        return createLigandMachineMmCIFID(self.getMmCIFChain(), self.getResName(),
                              self.getMmCIFResNum(), sep=sep)

    def getOSTID(self):
        """ Get a machine-readable ligand ID. Without Alt code."""
        return createLigandMmCIFID(self.getMmCIFChain(), self.getResName(),
                              self.getOSTResNum())

    def getMachineOSTID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code."""
        return createLigandMachineMmCIFID(self.getMmCIFChain(), self.getResName(),
                              self.getOSTResNum(), sep=sep)
    
    def getAuthID(self):
        """ Get a machine-readable ligand ID. Without Alt code.""" 
        return createLigandAuthID(self.getAuthChain(), self.getResName(),
                              self.getAuthResNum(), self.getInsertionCode())
    
    def getMachineAuthID(self, sep=":"):
        """ Get a machine-readable ligand ID, with fields separated by 'sep'. Without Alt code.""" 
        return createLigandMachineAuthID(self.getAuthChain(), self.getResName(),
                              self.getAuthResNum(), self.getInsertionCode(),
                              sep=sep)


class PDBX():
    " A convenience class around RCSB's mmCIF PdbxReader"
    
    @staticmethod
    def CreateFromGZIP(file):
        """ Create from a gzipped mmCIF file """
        return PDBX(gzip.open(file, 'rt', encoding='utf-8'))
        
    def __init__(self, file):
        " Reads the CIF file with RCSB's PdbxReader"
        pr = PdbxReader(file)
        data = []
        pr.read(data)
        assert len(data) == 1, \
            RuntimeError("Expected to read exactly 1 DataContainer block with PdbxReader from %s" % file.name)
        self.pdbx = data[0]
        self.atom_sites = None
        self.filename = file
        
    def getObjAsDF(self, name):
        obj = self.getObj(name)
        return pd.DataFrame(obj.getRowList(), columns=obj.getAttributeList())
    
    """ Copy some behavior of PdbxReader object """
    def getObj(self, name):
        obj = self.pdbx.getObj(name)
        if obj is None:
            raise KeyError(name)
        return obj
    
    def exists(self, name):
        return self.pdbx.exists(name)
    
    def _loadAtomSites(self):
        try:
            self.atom_sites = PDBXAtomSites(self.getObjAsDF("atom_site"))
        except AssertionError as e:
            ost.LogError("Assertion error while reading atom site from: ", self.filename)
            raise e

    def getAtomSites(self):
        """ Returns a PDBXAtomSites object containing complete atom_sites
        with all atoms of the mmCIF file"""
        if self.atom_sites is None:
            self._loadAtomSites()
        return self.atom_sites

