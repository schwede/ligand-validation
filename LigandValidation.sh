#!/bin/sh

# 5MKA fails because wrong validation file
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-07-15T05-00/2017-07-15_00000045/5mka.cif.gz /scicore/home/schwede/cameo/evaluation/2017-07-15T05-00/2017-07-15_00000045/5mka_validation.xml.gz -

# Passes
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-06-02T05-00/2018-06-02_00000188/6gmd.cif.gz /scicore/home/schwede/cameo/evaluation/2018-06-02T05-00/2018-06-02_00000188/6gmd_validation.xml.gz -

# Percent RSRZ, RFree, Percent rama, resolution
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-04-01T11-04/2017-04-01_00000144/5uhi.cif.gz /scicore/home/schwede/cameo/evaluation/2017-04-01T11-04/2017-04-01_00000144/5uhi_validation.xml.gz -

# Resolution, Resolution diff, R, Rfree
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-06-24T05-00/2017-06-24_00000144/5o1o.cif.gz /scicore/home/schwede/cameo/evaluation/2017-06-24T05-00/2017-06-24_00000144/5o1o_validation.xml.gz -

# Missing R, Rfree, percent RSRZ
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000021/5ou1.cif.gz /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000021/5ou1_validation.xml.gz -

# Missing percent-rama
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-08-19T05-00/2017-08-19_00000036/5ngq.cif.gz /scicore/home/schwede/cameo/evaluation/2017-08-19T05-00/2017-08-19_00000036/5ngq_validation.xml.gz -

# OST sloppy
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-12-16T05-00/2017-12-16_00000062/5mhi.cif.gz /scicore/home/schwede/cameo/evaluation/2017-12-16T05-00/2017-12-16_00000062/5mhi_validation.xml.gz -

# Failed to reproduce R values, missing percent-RSRZ-outliers, Rfree and R
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000023/5ou3.cif.gz /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000023/5ou3_validation.xml.gz -

# Ligand filtered with RSCC/B Factors
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-08-05T05-00/2017-08-05_00000191/5ww5.cif.gz /scicore/home/schwede/cameo/evaluation/2017-08-05T05-00/2017-08-05_00000191/5ww5_validation.xml.gz -

# Atom counts/OST
# Alternative config A-B/C-D in same residue
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-08-12T05-00/2017-08-12_00000061/5lvd.cif.gz /scicore/home/schwede/cameo/evaluation/2017-08-12T05-00/2017-08-12_00000061/5lvd_validation.xml.gz -
# Correctly read A only
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-08-12T05-00/2017-08-12_00000071/5ma7.cif.gz /scicore/home/schwede/cameo/evaluation/2017-08-12T05-00/2017-08-12_00000071/5ma7_validation.xml.gz -

# Outliers
# density, chirality, clashes
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-12-09T05-00/2017-12-09_00000032/5on1.cif.gz /scicore/home/schwede/cameo/evaluation/2017-12-09T05-00/2017-12-09_00000032/5on1_validation.xml.gz -
# geometry, density, clashes
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-01-06T05-00/2018-01-06_00000130/6bbl.cif.gz /scicore/home/schwede/cameo/evaluation/2018-01-06T05-00/2018-01-06_00000130/6bbl_validation.xml.gz -

# Null occupancy
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000067/5xw9.cif.gz /scicore/home/schwede/cameo/evaluation/2018-03-24T04-00/2018-03-24_00000067/5xw9_validation.xml.gz -

# Missing Proximity ModelledSubgroups
PDBValidation /scicore/home/schwede/cameo/evaluation/2017-12-02T05-00/2017-12-02_00000267/6ekc.cif.gz /scicore/home/schwede/cameo/evaluation/2017-12-02T05-00/2017-12-02_00000267/6ekc_validation.xml.gz -

# Clashing partial occupancy
PDBValidation /scicore/home/schwede/cameo/evaluation/2018-04-21T05-00/2018-04-21_00000121/6d0g.cif.gz /scicore/home/schwede/cameo/evaluation/2018-04-21T05-00/2018-04-21_00000121/6d0g_validation.xml.gz -
